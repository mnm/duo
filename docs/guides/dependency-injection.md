---
---

# Dependency Injection

Dependency injection (DI) is a powerful technique for building testable, modular software. By building larger components from smaller components, you create seams through your application for your dependencies to be swapped, wrapped and reconfigured for different environments.

Let's start with a basic example of an API server that uses DI to build itself up:

```go
func main() {
  env := env.Load()
  log := log.New(env)
  db := postgres.Connect(env, log)
  mc := mailchimp.New(env, log)
  mailer := mailer.New(mc, log)
  mailq := queue.New(mailer, log)
  api := api.New(env, log, db, mailq)
  api.ListenAndServe(":8080")
}
```

Conceptually you can think of DI as a directed acyclic graph (DAG) of dependencies:

```
MONODRAW of graph
```

Each one of these arrows is a seam that you can swap out for another component. Prefer Mailchimp to Mailgun? Easy. Swap out that dependency:

```diff
func main() {
  env := env.Load()
  log := log.New(env)
  db := postgres.Connect(env, log)
- mc := mailchimp.New(env, log)
+ mg := mailgun.New(env, log)
  mailer := mailer.New(mc, log)
  mailq := queue.New(mailer, log)
  api := api.New(env, log, db, mailq)
  api.ListenAndServe(":8080")
}
```

For testing, you don't want to be sending test emails to your customers. DI also makes it easy to swap out dependencies for specific environments:

```diff
func testAPI() http.Handler {
  env := env.Load()
  log := log.New(env)
  db := postgres.Connect(env, log)
- mg := mailgun.New(env, log)
+ mv := mailvoid.New()
  mailer := mailer.New(mv, log)
  mailq := queue.New(mailer, log)
  api := api.New(env, log, db, mailq)
  return api
}
```

Unfortunately once you factor in error handling, environments and more dependencies, you can easily have hundreds of lines of repetitive initialization code sprinkled across your codebase.

Thankfully, Duo automates dependency injection for you! You just declare and use your dependencies. Duo takes care of wiring them up.

<!-- EXAMPLE HERE -->

## Providing Dependencies

A dependency can be any concrete type declaration anywhere in your project directory:

```go
type Dependency struct {}
type Dependency string
type Dependency = AnotherDependency
```

> TODO: This is a weak intro. Also add a note about why only in project directory (search space is smaller)

Type definitions on their own aren't very useful. More often, you return dependencies from functions. These functions may wrap third-party libraries to provide application specific configuration.

Let's see how to define a `Log` dependency that writes to `stderr`:

```go
package console

func New() *Logger {
  return &Logger{os.Stderr}
}

type Log struct {
  writer os.Writer
}

func (log *Log) Info(msg []byte) {
  log.writer.Write(msg)
}
```

Looks like regular Go code, right? That's the goal. You shouldn't need to change how you program to work with Duo dependency injection system.

Dependencies can have zero or more sub-dependencies. Let's take it up a notch and define an `API` dependency that depends on `Log`.

Let's learn how to use the `Logger` dependency we defined above. Public function can depend on dependencies as parameters: \

```go
package api

import (
  ""
)

func New(log *console.Log) *API {
  return &API{log}
}

type API struct {
  log *console.Log
}
```

> TODO: finish DI example

Public fields in structs can also indicate a dependency:

```go
package api

type API struct {
  Log *console.Log
}
```

Of course public embedded structs also work:

```go
package api

type API struct {
  *console.Log
}
```

But private fields are not injected:

```go
package api

type API struct {
  *console.Log

  port int // ignored
}
```

## Depending on Other Types

You can't depend on built-in primitive types like `string` or `int` because Duo doesn't know how to initialize them.

However, you can depend on any user-defined data type:

```go
func Load() ID {
  return ID("some id")
}

type ID string

type API struct {
  ID ID // injected with Load()
}
```

You can also depend on type aliases:

```go
func Load() ID {
  return ID("some id")
}

type ID string
type Alias = ID

type API struct {
  ID Alias // injected with Load()
}
```

## Conflicting Providers

Many times there will be multiple ways to initialize a dependency. Consider the following example:

```go
func Stdout() *Logger {
  return &Logger{1, os.Stdout}
}

func StdoutWithLevel(level int) *Logger {
  return &Logger{level, os.Stdout}
}

func Stderr() *Logger {
  return &Logger{1, os.Stderr}
}

type Logger struct {
  level int
  writer io.Writer
}
```

There are 4 ways to initialize the `Logger`:

1. Call `Stdout()`
2. Call `StdoutWithLevel(int)`
3. Call `Stderr()`
4. Initialize the struct &`Logger{}`

Duo first filters out any ineligible candidates. In this case, `StdoutWithLevel(int)` is ineligible because it depends on the built-in `int` which we can't provide.

1. Call `Stdout()`
2. ~~Call `StdoutWithLevel(int)`~~
3. Call `Stderr()`
4. Initialize the struct &`Logger{}`

Duo prefers function providers over structs providers, so now we're left with `Stdout()` and `Stderr()`.

1. Call `Stdout()`
2. ~~Call `StdoutWithLevel(int)`~~
3. Call `Stderr()`
4. ~~Initialize the struct &`Logger{}` ~~

At this point, we just choose the first eligible provider that Duo finds in the package. Duo searches the files in a package alphanumerically. Within a file, Duo searches from top to bottom.

1. Call `Stdout()`
2. ~~Call `StdoutWithLevel(int)`~~
3. ~~Call `Stderr()`~~
4. ~~Initialize the struct &`Logger{}`~~

> TODO: Check if the parser searches packages in a file alphanumerically

> TODO: Figure out how dependency injection can inject the request logger over the global logger

## Error Handling

Many dependencies aren't as simple as the `Logger` dependency above. Consider the following dependency for loading the environment:

```go
package env

import (
  "github.com/caarlos0/env/v6"
)

func Load() (*Env, error) {
  e := new(Env)
  if err := env.Parse(e); err != nil {
    return nil, err
  }
  return e, nil
}

type Env struct {
  DatabaseURL   string `env:"DATABASE_URL,required"`
  SessionSecret string `env:"SESSION_SECRET,required"`
}
```

The environment fails to load if `DATABASE_URL` or `SESSION_SECRET` are not provided as environment variables.

Fortunately, Duo handles initialization errors transparently for you. You can use your dependencies without worrying about it.

```go
package api

import (
  "app.com/internal/env"
)

type API struct {
  *env.Env
}
```

If `SESSION_SECRET` is not defined when you boot the application, you'll get a runtime error:

```sh
env: required environment variable "SESSION_SECRET" is not set
```

## Passing in `context.Context`

You can optionally define a dependency with a context. The lifecycle of this context will depend on how it's used:

- **Initialization:** The context will be canceled by `SIGINT` or `SIGTERM`.
- **Request:** The context will be canceled if the request is cancelled

Let's see how we can use `context.Context` to safely open a connection to a Postgres database:

```go
package postgres

import (
  "context"
  "github.com/jackc/pgx/v4"
)

// Connect to a postgres database
func Connect(context.Context, env *env.Env) (*Client, error) {
  conn, err := pgx.Connect(ctx, env.DatabaseURL)
  if err != nil {
    return nil, err
  }
  return &Client{conn}, nil
}

// Client for postgres
type Client struct {
  *pgx.Conn
}
```

## Depending on Interfaces

You can swap dependencies by depending on interfaces. In the following example `Log` depends on an `io.Writer`.

```go
package console

import (
  "io"
)

type Log struct {
  io.Writer
}
```

Interfaces are unique because you can depend on an standard interface like `io.Writer` but the implementation could be defined almost anywhere: the standard library, your application or a dependency.

To make the search space smaller, Duo follows several heuristics for where the implementation of `io.Writer` could be located:

- `./writer`: The writer package in the project root. You should be cautious implementing dependencies here, because Duo may claim the `./writer` location in the future.
- `./internal/writer`: The writer package in `internal/`. This is the preferred place for implementing application-specific interfaces.

If there are packages in both locations, Duo prefers the `./internal/writer` package over the `./writer` package.

The directories searched are lowercase and omit special characters. Consider the following interface:

```go
type Pub_Sub interface {
  Publish()
  Subscribe()
}
```

Duo would search for an implementation first in `./internal/pubsub` then in `./pubsub`.

## Shutdown a Dependency

Each dependency can optionally define a `Close` function. This will be called at the end of a dependency's lifecycle. For example, you may have a database connect that you'd like to gracefully close down before the application exits.

```go
type Database struct {
}

func (db *Database) Close() error {
  // Called as the application is shutting down
  return nil
}
```

If the dependency depends on either the `*http.Request` or the `http.ResponseWriter`, the `Close` function may will be called after the response is written.

```go
type Log struct {
  *http.Request
}

func (l *Log) Close() error {
  // Flush the logs after the response
  return nil
}
```
