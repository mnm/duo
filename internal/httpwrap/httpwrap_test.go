package httpwrap_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	. "gitlab.com/mnm/duo/internal/httpwrap"
	"github.com/tj/assert"
)

func TestWrap(t *testing.T) {
	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rw := Wrap(w)
		defer rw.Flush()
		rw.WriteHeader(201)
		rw.Write([]byte(`hi world`))
		rw.Header().Add("Content-Type", "text/plain")
	}))
	defer s.Close()
	res, err := http.Get(s.URL)
	assert.NoError(t, err)
	defer res.Body.Close()
	assert.Equal(t, 201, res.StatusCode)
	body, err := ioutil.ReadAll(res.Body)
	assert.NoError(t, err)
	assert.Equal(t, "hi world", string(body))
	value := res.Header.Get("Content-Type")
	assert.Equal(t, "text/plain", value)
}

func TestUnwrap(t *testing.T) {
	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rw := Wrap(w)
		w = rw
		w.WriteHeader(201)
		w.Write([]byte(`hi world`))
		w.Header().Add("Content-Type", "text/plain")
		rw, ok := Unwrap(w)
		assert.True(t, ok)
		assert.Equal(t, 201, rw.Code)
		assert.Equal(t, string(`hi world`), rw.Body.String())
	}))
	defer s.Close()
	res, err := http.Get(s.URL)
	assert.NoError(t, err)
	defer res.Body.Close()
}
