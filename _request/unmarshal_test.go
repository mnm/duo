package request_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	request "gitlab.com/mnm/duo/request"
	"github.com/matthewmueller/diff"
	"github.com/tj/assert"
)

func Get(path string) *Request {
	return &Request{httptest.NewRequest(http.MethodGet, path, nil)}
}

func Post(path string, body string) *Request {
	return &Request{httptest.NewRequest(http.MethodPost, path, bytes.NewBufferString(body))}
}

type Request struct {
	*http.Request
}

func (r *Request) Set(key, value string) *Request {
	r.Header.Set(key, value)
	return r
}

func Test(t *testing.T) {
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			if test.todo {
				t.SkipNow()
			}
			// t.Parallel()
			err := request.Unmarshal(test.request.Request, &test.schema)
			if err != nil {
				if test.err != "" {
					assert.Equal(t, test.err, err.Error())
					return
				}
				assert.NoError(t, err)
			}
			actual, err := json.Marshal(test.schema)
			assert.NoError(t, err)
			diff.TestString(t, test.expect, string(actual))
		})
	}
}

var tests = []struct {
	todo    bool
	name    string
	request *Request
	schema  interface{}
	expect  string
	err     string
}{
	{
		name:    "get blank",
		request: Get("/"),
		schema:  &struct{}{},
		expect:  `{}`,
	},
	{
		name:    "get query pascal",
		request: Get("/?ID=10&Title=first"),
		schema: &struct {
			ID    int
			Title string
		}{},
		expect: `{"ID":10,"Title":"first"}`,
	},
	{
		name:    "get query lower",
		request: Get("/?id=10&title=first"),
		schema: &struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		}{},
		expect: `{"id":10,"title":"first"}`,
	},
	{
		todo:    true,
		name:    "get query sensitive",
		request: Get("/?id=10&title=first"),
		schema: &struct {
			ID    int
			Title string
		}{},
		err: ``,
	},
	{
		todo:    true,
		name:    "get query sensitive json",
		request: Get("/?ID=10&Title=first"),
		schema: &struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		}{},
		err: ``,
	},
	{
		name:    "get query ignore",
		request: Get("/?id=10&title=first&body=whatever"),
		schema: &struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		}{},
		expect: `{"id":10,"title":"first"}`,
	},
	{
		todo:    true,
		name:    "get query required missing",
		request: Get("/?id=10"),
		schema: &struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		}{},
		err: ``,
	},
	{
		name:    "get query float",
		request: Get("/?id=10.5"),
		schema: &struct {
			ID float64 `json:"id"`
		}{},
		expect: `{"id":10.5}`,
	},
	{
		name:    "post json no body",
		request: Post("/", "").Set("Content-Type", "application/json"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		expect: `{}`,
	},
	{
		name:    "post json empty",
		request: Post("/", `{}`).Set("Content-Type", "application/json"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		expect: `{}`,
	},
	{
		name:    "post json",
		request: Post("/", `{"A":"a", "B":"b"}`).Set("Content-Type", "application/json"),
		schema: &struct {
			A string
			B string
		}{},
		expect: `{"A":"a","B":"b"}`,
	},
	{
		name:    "post json tag",
		request: Post("/", `{"a":"a", "b":"b"}`).Set("Content-Type", "application/json"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		expect: `{"a":"a","b":"b"}`,
	},
	{
		name:    "post json empty no content type",
		request: Post("/", `{"a":"a", "b":"b"}`),
		schema:  &struct{}{},
		expect:  `{}`,
	},
	{
		todo:    true,
		name:    "post json no content type",
		request: Post("/", `{"a":"a", "b":"b"}`),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		err: ``,
	},
	{
		name:    "post json query",
		request: Post("/?a=a&b=b", `{"c":"c"}`).Set("Content-Type", "application/json"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
			C string `json:"c,omitempty"`
		}{},
		expect: `{"a":"a","b":"b","c":"c"}`,
	},
	{
		todo:    true,
		name:    "post json sensitive",
		request: Post("/", `{ "id":10, "title": "Duo" }`).Set("Content-Type", "application/json"),
		schema: &struct {
			ID    int
			Title string
		}{},
		err: ``,
	},
	{
		todo:    true,
		name:    "post json sensitive tag",
		request: Post("/", `{ "ID":10, "Title": "Duo" }`).Set("Content-Type", "application/json"),
		schema: &struct {
			ID    int    `json:"id,omitemtpy"`
			Title string `json:"title,omitempty"`
		}{},
		err: ``,
	},
	{
		name:    "post json query override",
		request: Post("/?a=a&b=b", `{"b":"c"}`).Set("Content-Type", "application/json"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		expect: `{"a":"a","b":"b"}`,
	},
	{
		name:    "post json float",
		request: Post("/", `{"A":10.5}`).Set("Content-Type", "application/json"),
		schema: &struct {
			A float64
		}{},
		expect: `{"A":10.5}`,
	},
	{
		name:    "post form no body",
		request: Post("/", "").Set("Content-Type", "application/x-www-form-urlencoded"),
		schema:  &struct{}{},
		expect:  `{}`,
	},
	{
		name:    "post form",
		request: Post("/", `A=a&B=b`).Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			A string
			B string
		}{},
		expect: `{"A":"a","B":"b"}`,
	},
	{
		name:    "post form tag",
		request: Post("/", `a=a&b=b`).Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		expect: `{"a":"a","b":"b"}`,
	},
	{
		todo:    true,
		name:    "post form no content type",
		request: Post("/", `a=a&b=b`),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		err: ``,
	},
	{
		name:    "post form empty no content type",
		request: Post("/", `a=a&b=b`),
		schema: &struct {
		}{},
		expect: `{}`,
	},
	{
		name:    "post form query",
		request: Post("/?a=a&b=b", `c=c`).Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
			C string `json:"c,omitempty"`
		}{},
		expect: `{"a":"a","b":"b","c":"c"}`,
	},
	{
		todo:    true,
		name:    "post form sensitive",
		request: Post("/", "id=10&title=Duo").Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			ID    int
			Title string
		}{},
		err: ``,
	},
	{
		todo:    true,
		name:    "post form sensitive tag",
		request: Post("/", "ID=10&Title=Duo").Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			ID    int    `json:"id,omitemtpy"`
			Title string `json:"title,omitempty"`
		}{},
		err: ``,
	},
	{
		name:    "post form ignore",
		request: Post("/", "id=10&title=duo&body=whatever").Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		}{},
		expect: `{"id":10,"title":"duo"}`,
	},
	{
		todo:    true,
		name:    "post form required missing",
		request: Post("/", "id=10").Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			ID    int    `json:"id"`
			Title string `json:"title"`
		}{},
		err: ``,
	},
	{
		name:    "post form query override",
		request: Post("/?a=a&b=b", `b=c`).Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			A string `json:"a,omitempty"`
			B string `json:"b,omitempty"`
		}{},
		expect: `{"a":"a","b":"b"}`,
	},
	{
		name:    "post form float",
		request: Post("/", `A=10.5`).Set("Content-Type", "application/x-www-form-urlencoded"),
		schema: &struct {
			A float64
		}{},
		expect: `{"A":10.5}`,
	},
}
