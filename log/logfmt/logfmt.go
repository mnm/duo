package logfmt

import (
	"io"

	"gitlab.com/mnm/duo/log"
)

// New logger
func New(w io.Writer, l log.Level) log.Logger {
	return &logfmt{l, w}
}

// logfmt struct
type logfmt struct {
	l log.Level
	w io.Writer
}

var _ log.Logger = (*logfmt)(nil)

func (l *logfmt) Log(in log.Input) {
	if in.Level < l.l {
		return
	}
	// TODO: finish this up...
	l.w.Write([]byte(in.Message))
	return
}
