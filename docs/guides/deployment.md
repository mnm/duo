# Deploying to Production

## Automatic HTTPS Support

HTTPS is a requirement for all websites these days, but setting it up isn't always straightforward. Thankfully, Duo makes HTTPS easy.

HTTPS certificates are generated on the fly and cached for future requests. The workflow is the same for development and production.

### Development

In development, Duo relies on the Filippo's excellent [mkcert](https://mkcert.dev/) package for setting up HTTPS on localhost. Once you've installed `mkcert`, run the following command in your terminal:

```sh
$ mkcert -install
```

This will install a certificate authority (CA) locally. You only need to do this once. With the trusted CA in place, Duo can now generate locally-trusted certificates:

Now in the `web` package:

```
myapp
└── web
    └── web.go
```

Create a `Certificate` function that loads from `localhost`:

```go
package web

import (
  "gitlab.com/mnm/duo/certificate"
)

func Certificate() (certificate.Manager, error) {
  return certificate.Load("localhost")
}
```

#### Production

For production, you just need to alter the domain. The `Certificate` function supports automatic Dependency Injection, so it's straightforward to pass an environment through:

```go
package web

import (
  "gitlab.com/mnm/duo/certificate"
)

func Certificate(env *env.Env) (certificate.Manager, error) {
  return certificate.Load(env.Domain)
}
```

By default, the `certificate` package uses [LetsEncrypt](https://letsencrypt.org/) to generate and cache certificates as needed.

## HTTP/2 Support

Once you setup [HTTPS Support](#Automatic-HTTPS-Support), your server can also respond to HTTP/2 requests.

Thanks to Go's built-in HTTP/2 support, there's nothing more to do. Test support by curling your server:

```sh
$ curl -sI https://localhost:3000 -o/dev/null -w '%{http_version}\n'
2
```

## Graceful Shutdown

Duo can wait for active connections to finish before shutting down the server. To shutdown the server gracefully, send either a `SIGINT` or `SIGTERM` interrupt. This will start the shutdown process and the server stop accepting new connections and finish handling active requests.

Sometimes it can take awhile for connections to drain. To force the server to shutdown immediately, you can send an additional `SIGINT` or `SIGTERM`. This will abruptly terminate active connections and exit swiftly.

## Zero-Downtime Deploys

Duo ships with built-in socket activation to perform zero-downtime deploys. Instead of the web server listening on a port directly, a parent process will listen on that port and pass the listener into the web server. This way when we upgrade the web server process, the listener is still listening, queueing up requests. When the upgraded server is ready, it can process the queued requests. No requests are lost. Socket activation is not available on Windows.

To accept a listener, set the `LISTEN_FDS` and `LISTEN_PID` environment variables and pass in the socket.

Here's a minimal example to test this:

```go
package main

func main() {
if err := run("./main"); err != nil {
  log.Fatal("error running process: %+s", err)
}
}

// Spawn the server and pass in the socket
func run(serverPath string) error {
socket, err := Socket(":8000")
  if err != nil {
    return err
  }
  defer socket.Close()
  file, err := socket.File()
  if err != nil {
    return err
  }
  defer file.Close()
  cmd := exec.Command(serverPath)
  cmd.Env = append(os.Environ(), "LISTEN_FDS=1")
  cmd.Stdin = os.Stdin
  cmd.Stdout = os.Stdout
  cmd.Stderr = os.Stderr
  cmd.ExtraFiles = append(cmd.ExtraFiles, file)
  return cmd.Wait()
}

// Socket fn
func Socket(address string) (*net.TCPListener, error) {
  addr, err := net.ResolveTCPAddr("tcp", address)
  if err != nil {
    return nil, err
  }
  socket, err := net.ListenTCP("tcp", addr)
  if err != nil {
    return nil, err
  }
  return socket, nil
}
```

> TODO: finish zero-downtime section

### Zero-Downtime with Systemd

In production, you can use systemd to initialize the socket and pass the it into your web server. This is seamless in Duo:

```sh
cat /etc/systemd/system/myapp.service
```

```toml
[Unit]
Description=Hello socket

[Socket]
ListenStream=80
ListenStream=443
NoDelay=true
```

```sh
cat /etc/systemd/system/myapp.socket
```

```toml
[Unit]
Description=Hello socket

[Socket]
ListenStream=80
NoDelay=true
```

```sh
$ sudo systemctl stop hello.socket hello
```

> TODO: finish this example using [SystemD Socket Activation in Go](https://www.darkcoding.net/software/systemd-socket-activation-in-go).

## Tweaking the Server

The default web server ships with many of the best practices outlined in Cloudflare's definitive [So you want to expose Go on the Internet](https://blog.cloudflare.com/exposing-go-on-the-internet/). Some of this configuration is opinionated so you may want to adjust the defaults for your application's needs.

You can tweak your web server in the `web` package.

```
myapp
└── web
    └── web.go
```

Simply define a `Server` function that accepts an `*http.Server`:

```go
package web

func Server(server *http.Server) {
  server.ReadTimeout = 10 * time.Second
}
```

Your web server will be passed into this function before booting.

## `$PORT` Support

You can adjust the port the web server listens on by setting the `PORT` environment variable.

```sh
$ PORT=5000 duo run
```

This follows common conventions on cloud platforms like Heroku.

## Building a Single Binary

Preparing your application for production is as simple as running:

```sh
duo build
```

This command compiles your application into a single, self-contained binary that's ready to deploy. The `duo build` supports all the same flags and environment variables as `go build`.

For example, you can:

- Cross-compile your application by setting the `GOOS` and `GOARCH` environment variables.
- Change the binary output path with `duo build -o <path>`

## Deployment

After building your application, you're now ready to deploy it to production. Duo supports all the major cloud providers.

### Deploying to Heroku

> TODO Deploying to Heroku

### Deploying to Amazon Linux 2 and Centos 8

> TODO Deploying to Amazon Linux 2 and Centos 8

### Deploying to Ubuntu

> TODO Deploying to Ubuntu

### Deploying to Amazon Lambda

> TODO Deploying to Amazon Lambda
