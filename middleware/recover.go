package middleware

// // Recover middleware recovers from panics
// func Recover(log log.Log, views response.Views) Middleware {
// 	return func(next http.Handler) http.Handler {
// 		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 			defer func() {
// 				v := recover()
// 				if v != nil {
// 					res := response.New(r, views)
// 					err := fmt.Errorf("%s", v)
// 					res.Status(500).Error(err).Write(w)
// 				}
// 			}()
// 			next.ServeHTTP(w, r)
// 		})
// 	}
// }
