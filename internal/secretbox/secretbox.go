package secretbox

import (
	"crypto/rand"
	"crypto/sha256"
	"errors"
	"io"

	"golang.org/x/crypto/nacl/secretbox"
)

// Encode a message
func Encode(secret string, plaintext []byte) (ciphertext []byte, err error) {
	if len(secret) == 0 {
		return nil, errors.New("empty secret provided")
	} else if len(plaintext) == 0 {
		return nil, errors.New("empty plaintext provided")
	}
	nonce, err := Nonce()
	if err != nil {
		return nil, err
	}
	key := sha256.Sum256([]byte(secret))
	return secretbox.Seal(nonce[:], plaintext, &nonce, &key), nil
}

// Decode a message
func Decode(secret string, ciphertext []byte) (plaintext []byte, err error) {
	if len(secret) == 0 {
		return nil, errors.New("empty ciphertext provided")
	} else if len(ciphertext) == 0 {
		return nil, errors.New("empty ciphertext provided")
	}
	key := sha256.Sum256([]byte(secret))
	var nonce [24]byte
	copy(nonce[:], ciphertext[:24])
	plaintext, ok := secretbox.Open(nil, ciphertext[24:], &nonce, &key)
	if !ok {
		return nil, errors.New("failed to decrypt given message")
	}
	return plaintext, nil
}

// Generate generates a random secret for us to use
func Generate() ([32]byte, error) {
	var secret [32]byte
	_, err := io.ReadFull(rand.Reader, secret[:])
	return secret, err
}

// Nonce generates a random bytes. Override for testing
var Nonce = func() ([24]byte, error) {
	var nonce [24]byte
	_, err := io.ReadFull(rand.Reader, nonce[:])
	return nonce, err
}
