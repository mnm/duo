package dag

import (
	"fmt"
	"sort"
	"strings"
	"sync"
)

type Graph interface {
	Nodes() []string
	Outs(from string) (tos []string) // links out (dependencies)
	Ins(to string) (froms []string)  // links in (dependants)
	Link(from, to string)
	Set(path string)
	Remove(path string)
	String() string
}

func New() Graph {
	return &dag{
		nodes: map[string]struct{}{},
		ins:   map[string]map[string]struct{}{},
		outs:  map[string]map[string]struct{}{},
	}
}

type dag struct {
	mu    sync.RWMutex
	nodes map[string]struct{}
	ins   map[string]map[string]struct{}
	outs  map[string]map[string]struct{}
}

func (d *dag) Nodes() (nodes []string) {
	for path := range d.nodes {
		nodes = append(nodes, path)
	}
	sort.Strings(nodes)
	return nodes
}

func (d *dag) Set(path string) {
	d.mu.Lock()
	defer d.mu.Unlock()
	d.nodes[path] = struct{}{}
}

func (d *dag) Link(from, to string) {
	if from == to {
		return
	}
	d.mu.Lock()
	defer d.mu.Unlock()
	// Create nodes if we haven't yet
	d.nodes[from] = struct{}{}
	d.nodes[to] = struct{}{}
	// Set the dependencies
	if d.outs[from] == nil {
		d.outs[from] = map[string]struct{}{}
	}
	d.outs[from][to] = struct{}{}
	// Link the other way too
	if d.ins[to] == nil {
		d.ins[to] = map[string]struct{}{}
	}
	d.ins[to][from] = struct{}{}
}

// Remove a node
func (d *dag) Remove(path string) {
	// Remove the node
	// d.remove(path)
	// Publish the removal for listeners
	// d.ps.Publish(path, []byte(path))

}

// Unlink and remove the node
func (d *dag) remove(path string) {
	d.mu.Lock()
	defer d.mu.Unlock()
	// Remove dependant links
	for from := range d.ins[path] {
		delete(d.ins[path], from)
		delete(d.outs[from], path)
	}
	// Remove dependency links
	for to := range d.outs[path] {
		delete(d.outs[path], to)
		delete(d.ins[to], path)
	}
	// Remove node
	delete(d.nodes, path)
}

// Return the links out (dependencies)
func (d *dag) Outs(from string) (tos []string) {
	d.mu.RLock()
	defer d.mu.RUnlock()
	for to := range d.outs[from] {
		tos = append(tos, to)
	}
	return tos
}

// Return the links in (dependants)
func (d *dag) Ins(to string) (froms []string) {
	d.mu.RLock()
	defer d.mu.RUnlock()
	for from := range d.ins[to] {
		froms = append(froms, from)
	}
	return froms
}

func (d *dag) String() string {
	d.mu.RLock()
	defer d.mu.RUnlock()
	s := strings.Builder{}
	s.WriteString("digraph g {\n")
	for from := range d.nodes {
		tos := d.outs[from]
		if len(tos) == 0 {
			s.WriteString(fmt.Sprintf("  %q\n", from))
			continue
		}
		for to := range tos {
			s.WriteString(fmt.Sprintf("  %q -> %q\n", from, to))
		}
	}
	s.WriteString("}\n")
	return s.String()
}

// Ins recursively returns dependants, dependants of dependants, etc.
// Ins includes path.
func Ins(g Graph, path string) (descendants []string) {
	descendants = append(descendants, path)
	descendants = append(descendants, ins(g, path)...)
	return descendants
}

func ins(g Graph, path string) (descendants []string) {
	froms := g.Ins(path)
	for _, from := range froms {
		descendants = append(descendants, from)
		descendants = append(descendants, ins(g, from)...)
	}
	return descendants
}

// Outs recursively returns dependencies, dependencies of dependencies, etc.
// Outs includes self.
func Outs(g Graph, path string) (ancestors []string) {
	ancestors = append(ancestors, path)
	ancestors = append(ancestors, outs(g, path)...)
	return ancestors
}

func outs(g Graph, path string) (ancestors []string) {
	tos := g.Outs(path)
	for _, to := range tos {
		ancestors = append(ancestors, to)
		ancestors = append(ancestors, outs(g, to)...)
	}
	return ancestors
}
