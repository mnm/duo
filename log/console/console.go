package console

import (
	"fmt"
	"io"
	"os"
	"sync"

	"gitlab.com/mnm/duo/log"
)

// colors
const (
	none   = 0
	dim    = 2
	red    = 31
	green  = 32
	yellow = 33
	blue   = 34
	gray   = 30
)

// colors
var colors = [...]int{
	log.DebugLevel: gray,
	log.InfoLevel:  blue,
	log.WarnLevel:  yellow,
	log.ErrorLevel: red,
	log.FatalLevel: red,
}

// prefixes
var prefixes = [...]string{
	log.DebugLevel: "debug",
	log.InfoLevel:  "info",
	log.WarnLevel:  "warn",
	log.ErrorLevel: "error",
	log.FatalLevel: "fatal",
}

// New console writer
func New(w io.Writer, lvl log.Level) *Console {
	return &Console{Writer: w, Level: lvl}
}

// Console logger
type Console struct {
	Level  log.Level
	mu     sync.Mutex
	Writer io.Writer
}

var _ log.Logger = (*Console)(nil)

// Log implements Logger
func (c *Console) Log(log log.Input) {
	if c.Level > log.Level {
		return
	}
	color := colors[log.Level]
	level := prefixes[log.Level]

	c.mu.Lock()
	defer c.mu.Unlock()

	fmt.Fprintf(c.Writer, "\033[%dm%s\033[0m %s", color, level, log.Message)
	for _, key := range log.Fields.Keys() {
		fmt.Fprintf(c.Writer, " \033[%dm%s=%v\033[0m", dim, key, log.Fields.Get(key))
	}
	fmt.Fprintln(c.Writer)
}

// Default provides a default logger for di.
func Default() log.Log {
	return log.New(New(os.Stderr, log.InfoLevel))
}

// Stderr is a console log singleton that writes to stderr
var stderr = log.New(&Console{
	Level:  log.InfoLevel,
	Writer: os.Stderr,
})

// Field attaches a key/value pair and returns a new logger
func Field(key string, value interface{}) log.Log {
	return stderr.Field(key, value)
}

// Fields attaches a map of key/value pairs and returns a new logger
func Fields(fields map[string]interface{}) log.Log {
	return stderr.Fields(fields)
}

// Debug message is written to the console
func Debug(message string, args ...interface{}) {
	stderr.Debug(message, args...)
}

// Info message is written to the console
func Info(message string, args ...interface{}) {
	stderr.Info(message, args...)
}

// Notice message is written to the console
func Notice(message string, args ...interface{}) {
	stderr.Notice(message, args...)
}

// Warn message is written to the console
func Warn(message string, args ...interface{}) {
	stderr.Warn(message, args...)
}

// Error message is written to the console
func Error(message string, args ...interface{}) {
	stderr.Error(message, args...)
}

// Fatal message is written to the console
func Fatal(message string, args ...interface{}) {
	stderr.Fatal(message, args...)
}
