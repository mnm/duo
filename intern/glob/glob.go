package glob

import (
	"io/fs"

	"github.com/gobwas/glob"
)

func Match(fsys fs.FS, pattern string) (files []string, err error) {
	glob, err := glob.Compile(pattern)
	if err != nil {
		return nil, err
	}
	dir := Base(pattern)
	err = fs.WalkDir(fsys, dir, func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		// Match against the glob
		if glob.Match(path) {
			files = append(files, path)
		}
		return nil
	})
	return files, err
}

var Compile = glob.Compile
var MustCompile = glob.MustCompile
