package dom_test

import (
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"testing/fstest"

	"gitlab.com/mnm/duo/intern/vfs"
	"gitlab.com/mnm/duo/internal/npm"

	"github.com/matryer/is"
	"gitlab.com/mnm/duo/runtime/dfs"
	"gitlab.com/mnm/duo/runtime/dom"
	"gitlab.com/mnm/duo/runtime/svelte"

	v8 "gitlab.com/mnm/duo/js/v8"
)

func TestRunner(t *testing.T) {
	is := is.New(t)
	cwd, err := os.Getwd()
	is.NoErr(err)
	dir := filepath.Join(cwd, "_tmp")
	is.NoErr(os.RemoveAll(dir))
	defer func() {
		if !t.Failed() {
			is.NoErr(os.RemoveAll(dir))
		}
	}()
	memfs := vfs.Memory{
		"view/index.svelte": &fstest.MapFile{
			Data: []byte(`<h1>index</h1>`),
		},
		"view/about/index.svelte": &fstest.MapFile{
			Data: []byte(`<h2>about</h2>`),
		},
	}
	is.NoErr(vfs.WriteAll(".", dir, memfs))
	dirfs := os.DirFS(dir)
	svelte := svelte.New(&svelte.Input{
		VM:  v8.New(),
		Dev: true,
	})
	dfs := dfs.New(dirfs, map[string]dfs.Generator{
		"duo/view": dom.Runner(svelte, dir),
	})
	// Read the wrapped version of index.svelte with node_modules rewritten
	code, err := fs.ReadFile(dfs, "duo/view/_index.svelte")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `from "/duo/node_modules/svelte/internal"`))
	is.True(strings.Contains(string(code), `element("h1");`))
	is.True(strings.Contains(string(code), `text("index")`))
	is.True(strings.Contains(string(code), `"/duo/view/index.svelte": view_default`))
	is.True(strings.Contains(string(code), `page: "/duo/view/index.svelte",`))
	is.True(strings.Contains(string(code), `hot: new Hot("/duo/hot?page=%2Fduo%2Fview%2Findex.svelte", components)`))

	// Unwrapped version with node_modules rewritten
	code, err = fs.ReadFile(dfs, "duo/view/index.svelte")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `from "/duo/node_modules/svelte/internal"`))
	is.True(strings.Contains(string(code), `element("h1");`))
	is.True(strings.Contains(string(code), `text("index")`))
	// Unwrapped version doesn't contain wrapping
	is.True(!strings.Contains(string(code), `"/duo/view/index.svelte": view_default`))
	is.True(!strings.Contains(string(code), `page: "/duo/view/index.svelte",`))
	is.True(!strings.Contains(string(code), `hot: new Hot("/duo/hot?page=%2Fduo%2Fview%2Findex.svelte", components)`))

	// Read the wrapped version of about/index.svelte with node_modules rewritten
	code, err = fs.ReadFile(dfs, "duo/view/about/_index.svelte")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `from "/duo/node_modules/svelte/internal"`))
	is.True(strings.Contains(string(code), `element("h2");`))
	is.True(strings.Contains(string(code), `text("about")`))
	is.True(strings.Contains(string(code), `"/duo/view/about/index.svelte": about_default`))
	is.True(strings.Contains(string(code), `page: "/duo/view/about/index.svelte",`))
	is.True(strings.Contains(string(code), `hot: new Hot("/duo/hot?page=%2Fduo%2Fview%2Fabout%2Findex.svelte", components)`))

	// Unwrapped version with node_modules rewritten
	code, err = fs.ReadFile(dfs, "duo/view/about/index.svelte")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `from "/duo/node_modules/svelte/internal"`))
	is.True(strings.Contains(string(code), `element("h2");`))
	is.True(strings.Contains(string(code), `text("about")`))
	// Unwrapped version doesn't contain wrapping
	is.True(!strings.Contains(string(code), `"/duo/view/about/index.svelte": about_default`))
	is.True(!strings.Contains(string(code), `page: "/duo/view/about/index.svelte",`))
	is.True(!strings.Contains(string(code), `hot: new Hot("/duo/hot?page=%2Fduo%2Fview%2Fabout%2Findex.svelte", components)`))
}

func TestNodeModules(t *testing.T) {
	is := is.New(t)
	cwd, err := os.Getwd()
	is.NoErr(err)
	dir := filepath.Join(cwd, "_tmp")
	is.NoErr(os.RemoveAll(dir))
	defer func() {
		if !t.Failed() {
			is.NoErr(os.RemoveAll(dir))
		}
	}()
	memfs := vfs.Memory{
		"view/index.svelte": &fstest.MapFile{
			Data: []byte(`<h1>hi world</h1>`),
		},
	}
	is.NoErr(vfs.WriteAll(".", dir, memfs))
	dirfs := os.DirFS(dir)
	err = npm.Install(dir, "svelte@3.42.3")
	is.NoErr(err)
	dfs := dfs.New(dirfs, map[string]dfs.Generator{
		"duo/node_modules": dom.NodeModules(dir),
	})
	// Read the re-written node_modules
	code, err := fs.ReadFile(dfs, "duo/node_modules/svelte/internal")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `function element(`))
	is.True(strings.Contains(string(code), `function text(`))
}

func TestBuilder(t *testing.T) {
	is := is.New(t)
	cwd, err := os.Getwd()
	is.NoErr(err)
	dir := filepath.Join(cwd, "_tmp")
	is.NoErr(os.RemoveAll(dir))
	defer func() {
		if !t.Failed() {
			is.NoErr(os.RemoveAll(dir))
		}
	}()
	memfs := vfs.Memory{
		"view/index.svelte": &fstest.MapFile{
			Data: []byte(`<h1>index</h1>`),
		},
		"view/about/index.svelte": &fstest.MapFile{
			Data: []byte(`<h2>about</h2>`),
		},
	}
	is.NoErr(vfs.WriteAll(".", dir, memfs))
	dirfs := os.DirFS(dir)
	err = npm.Install(dir, "svelte@3.42.3")
	is.NoErr(err)
	err = npm.Link("../../duojs", dir)
	is.NoErr(err)
	svelte := svelte.New(&svelte.Input{
		VM:  v8.New(),
		Dev: true,
	})
	dfs := dfs.New(dirfs, map[string]dfs.Generator{
		"duo/view": dom.Builder(svelte, dir),
	})
	des, err := fs.ReadDir(dfs, "duo/view")
	is.NoErr(err)
	is.Equal(len(des), 3)
	is.Equal(des[0].Name(), "_index.svelte")
	is.Equal(des[0].IsDir(), false)
	is.Equal(des[1].Name(), "about")
	is.Equal(des[1].IsDir(), true)
	is.Equal(des[2].Name(), "chunk-ELAYS67D.js")
	is.Equal(des[2].IsDir(), false)
	des, err = fs.ReadDir(dfs, "duo/view/about")
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "_index.svelte")
	is.Equal(des[0].IsDir(), false)

	code, err := fs.ReadFile(dfs, "duo/view/_index.svelte")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `"H1"`))
	is.True(strings.Contains(string(code), `"index"`))
	is.True(strings.Contains(string(code), `from"./chunk-ELAYS67D.js"`))
	is.True(strings.Contains(string(code), `page:"/duo/view/index.svelte"`))
	is.True(strings.Contains(string(code), `document.getElementById("duo_target")`))
	// TODO: remove hot
	// is.True(!strings.Contains(string(code), `hot:`))

	code, err = fs.ReadFile(dfs, "duo/view/about/_index.svelte")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `"H2"`))
	is.True(strings.Contains(string(code), `"about"`))
	is.True(strings.Contains(string(code), `from"../chunk-ELAYS67D.js"`))
	is.True(strings.Contains(string(code), `page:"/duo/view/about/index.svelte"`))
	is.True(strings.Contains(string(code), `document.getElementById("duo_target")`))
	// TODO: remove hot
	// is.True(!strings.Contains(string(code), `hot:`))

	code, err = fs.ReadFile(dfs, "duo/view/chunk-ELAYS67D.js")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `"allowpaymentrequest"`))
	is.True(strings.Contains(string(code), `"readonly"`))
}
