---
---

# Sessions

HTTP is a stateless protocol on it's own – you can't tell who made which request. Sessions are built on top of HTTP to link requests to users. Sessions provide [Authentication](./Authentication) and [Authorization](./Authorization).

You can add sessions to your application by defining a `Session` struct inside your application's `session` package:

```
myapp
└── session
   └── session.go
```

And inside `session.go`:

```go
package session

// Session data
type Session struct {
  ID *string
}
```

With the session now defined, the `Session` data will now be persistent for each user across requests using an encrypted HTTP cookie.

![Browser Storage Tab](./media/browser-storage.png)

You can now use this session in your controllers:

```go
package controller

type Controller struct {
  Session *session.Session
}

func (c *Controller) Index() {}
```

## Extending Sessions

Within the `session` package, you can extend the session by building types that depend on the `Session`. Types like `User` below. These types can be initialized using functions that support automatic Dependency Injection.

```go
type Session struct {
  ID *int
}

func LoadUser(db *sql.Database, session *Session) (*User, error) {
  if session.ID == nil {
    return nil, nil
  }
  // hypothetical database call
  return db.FindUserByID(*session.ID)
}

type User struct {
  *table.User
}
```

You can then depend on the User from your controllers

```go
package controller

type Controller struct {
  Session *session.User
}

func (c *Controller) Index() {}
```

This helps cut down on the boilerplate. Instead of loading dependent data in each controller, you can define that logic once in the session package and share those types across your controllers.

You also have access to the `*http.Request` and `http.ResponseWriter`, so it's easy to do this...

> TODO: add example with using request and response in the session here

## Authentication Example

Now that we've defined a user session, we can use that session in a controller. To demonstrate this, let's implement login and logout actions. To do this, I'll create a `session` controller, but the name of the controller doesn't matter:

```
myapp
├── controller
│   └── session
│       └── controller.go
└── session
    └── user.go
```

Here's what the `session` controller might look like:

```go
package session

type Controller struct {
  Session *session.Session
}

// Login by creating a session
func (c *Controller) Create(email, password string) error {
  // Authenticate the user (using SQL, an ORM, etc.)
  user, err := authenticateUser(email, password)
  if err != nil {
    return err
  }
  // Store the user ID in the session
  c.Session.ID = &user.ID
  return nil
}

// Logout by deleting the session
func (c *Controller) Delete() {
  c.Session.ID = nil
}
```

### Visitors

Sessions are broader than signup, login and logout. Session simply link requests to a user. You don't need to be logged in for sessions to be useful.

To see this in action, let's add a property that applies to everyone, even visitors:

```go
// Session data
type Session struct {
  LastVisited *int
}
```

`LastVisited` defines the number of seconds since the last visit. `nil` means they haven't visited before.

```go
package posts

type Controller struct {
  User *session.User
}

func (c *Controller) Show(id int) (*Post, error) {
  now := time.Now().Unix()
  if c.User.LastVisited == nil {
    fmt.Println("first time visitor!")
    c.User.LastVisited = now
  } else {
    fmt.Println("last visited", now.Sub(c.User.LastVisited))
    c.User.LastVisited = now
  }
}
```

> TODO: cleanup this example now that we've explained extending the session.

In the code above, we first check if the request is from a first-time visitor, otherwise we take the difference between now and the last time they visited the website. The example above works, but adding this logic to every action is messy and repetitive. Thankfully there's a better way.

## Authorization Example

While authentication links a request to a user, authorization defines what a user is allowed to do. A blog is a good example: every reader can read a blog post, but only editors can edit a blog post.

We can implement authorization easily by expanding the `User` struct in the `session` package:

```go
package session

// User session
type User struct {
  ID      *string
  CanEdit bool
}
```

Now when someone logs in, we can set use the `CanEdit` boolean to decide whether or not they can edit the blog

```go
package posts

type Controller struct {
  User *session.User
}

func (c *Controller) Edit(id int, post string) (*Post, error) {
  if !c.User.CanEdit {
    return nil, errors.New("You're not allowed to edit blog posts")
  }
  return nil, nil
}

func (c *Controller) Update(id int, post string) error {
  if !c.User.CanEdit {
    return nil, errors.New("You're not allowed to update blog posts")
  }
}
```
