package dag_test

import (
	"fmt"
	"testing"

	"gitlab.com/mnm/duo/intern/dag"
)

func TestRemove(t *testing.T) {
	graph := dag.New()
	graph.Link("go.mod", "duo/main.go")
	graph.Link("duo/view/_ssr.js", "node_modules/react-dom/server.browser.js")
	graph.Link("duo/view/_ssr.js", "node_modules/object-assign/index.js")
	graph.Link("duo/view/_ssr.js", "modules/youtube/index.ts")
	graph.Link("duo/view/_ssr.js", "view/index.jsx")
	graph.Link("duo/view/_ssr.js", "modules/uid/index.ts")
	graph.Link("duo/view/_ssr.js", "node_modules/react/cjs/react.development.js")
	graph.Link("duo/view/_ssr.js", "node_modules/react/index.js")
	graph.Link("duo/view/_ssr.js", "node_modules/react-dom/cjs/react-dom-server.browser.development.js")
	graph.Link("duo/main.go", "duo/program/program.go")
	graph.Link("duo/program/program.go", "duo/web/web.go")
	fmt.Println(graph.String())
	for _, in := range dag.Ins(graph, "modules/uid/index.ts") {
		graph.Remove(in)
	}
	fmt.Println(graph.String())
}
