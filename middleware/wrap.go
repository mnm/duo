package middleware

import (
	"net/http"

	"gitlab.com/mnm/duo/internal/httpwrap"
)

// Wrap the response in middleware to allow us to safely set HTTP headers in
// other middleware without worrying about the response being written by the
// controller
func Wrap() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			rw := httpwrap.Wrap(w)
			next.ServeHTTP(rw, r)
			rw.Flush()
		})
	}
}
