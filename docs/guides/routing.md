---
---

# Routing

Routing is how a request reaches an action. Duo starts off with simple default routing that you can customize and evolve over time.

## Directory Routing

The layout of the `controller/` directory influences the default routing.

```
myapp
    └── controller
        ├── controller.go          -> /
        ├── posts
        │   ├── posts.go           -> /posts
        │   └── comments
        │       └── comments.go    -> /posts/:post_id/comments
        └── users
            └── users.go           -> /users
```

You can extend, override and disable directory-based routing with [Custom Routing](./Custom-Routing).

## Default Routing

In every controller, you can define **resourceful actions**. Resourceful actions are special actions help you manage the lifecycle of a resource. These actions have default routes. There are 7 resourceful actions and they're listed below:

| Action   | Description        |
| :------- | :----------------- |
| `Index`  | List a resource    |
| `New`    | New resource form  |
| `Create` | Create a resource  |
| `Show`   | Show a resource    |
| `Edit`   | Edit resource form |
| `Update` | Update a resource  |
| `Delete` | Delete a resource  |

Here's how default routing applies to a `users` controller:

| Method   | Path              | Action | Description      |
| :------- | :---------------- | :----- | :--------------- |
| `GET`    | `/users`          | Index  | List users       |
| `GET`    | `/users/new`      | New    | New user form    |
| `POST`   | `/users`          | Create | Create a user    |
| `GET`    | `/users/:id`      | Show   | Show a user      |
| `GET`    | `/users/:id/edit` | Edit   | Edit a user form |
| `PATCH`  | `/users/:id`      | Update | Update a user    |
| `DELETE` | `/users/:id`      | Delete | Delete a user    |

Nested controllers like the `comments` controller follow a similar pattern:

| Method | Path | Action | Description |
| :-- | :-- | :-- | :-- |
| `GET` | `/posts/:post_id/comments` | Index | List comments |
| `GET` | `/posts/:post_id/comments/new` | New | New comment form |
| `POST` | `/posts/:post_id/comments` | Create | Create a comment |
| `GET` | `/posts/:post_id/comments/:id` | Show | Show a comment |
| `GET` | `/posts/:post_id/comments/:id/edit` | Edit | Edit a comment form |
| `PATCH` | `/posts/:post_id/comments/:id` | Update | Update a comment |
| `DELETE` | `/posts/:post_id/comments/:id` | Delete | Delete a comment |

Of course, you can override the default routing with [Custom Routing](./Custom-Routing).

All other actions do not have a default route:

```go
func About() {}
func Deactivate() {}
```

You can route requests to your `About` and `Deactivate` actions with [Custom Routing](./Custom-Routing).

## Multi-format Responses

> TODO

You have two ways of specifying the file format.

### Adding the File Extension

> TODO `/users/10.json`

### Content Negotiation

> TODO `Accepts: application/json`

## Custom Routing

Sometimes file-based routing isn't quite flexible enough.

```go
var Routes = route.Map{
	// Remap controllers
	"/":         "posts",
	"/comments": "posts/comments",

	// Remap actions
	"PUT /":                      "posts.Update",
	"GET /archives/:year/:month": "posts/archives.Show",

  // Disable admin (or maybe "404"?)
  "/admin": ""
}
```

## Dynamic Routing

> TODO: Custom routing will replace this section. In order to generate clients, we need to know the routes at build time. That's not easily possible with routing that's not known until runtime.

The action and file-based routing described above can be extended, overridden, and disabled. Take control of how requests are routed by defining a `Handler` function in the `web` package.

```
myapp
└── web
    └── web.go
```

The `Handler` function must return an `http.Handler`. `Handler` supports [Dependency Injection](./dependency-injection). Let's start with the default `Handler` function:

```go
package web

import (
  "myapp.com/duo/controller"
)

func Handler(router controller.Router) http.Handler {
  router.Get("/comments", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

  })
  return router
}
```

### Controller Router

The `controller.Router` is a generated type that already contains our action routing. The router also implements the `http.Handler` interface so we can return it directly. So far we haven't changed any behavior. Let's start customizing our routes.

### Adding and Changing Routes

> TODO: Finish

```go
package web

func Handler(router controller.Router, c controller.Directory) http.Handler {
  router.Get("/about", c.Users.About)
  router.Post("/deactivate", c.Users.Deactivate)
  router.Get("/archive/:year/:month", c.Users.Archive)
  router.Post("/users", c.Users.Create)
  router.Get("/users/:id", c.Users.Show)
  return router
}
```

#### Controller Directory

> TODO: Controller Directory

#### Adding Routes

> TODO: Adding routes

#### Changing Default Routes

When you assign a route to an action that already has a [default route](#Default_Routing), we disable the default route.

<!-- Diagram of move from one command to another -->

So if we have the following route in the `Handler` function, the `Create` action would no longer be accessible to `POST /users`:

```go
router.Post("/myusers", c.Users.Create)
```

To additionally support `POST /users`, you could do:

```go
router.Post("/myusers", c.Users.Create)
router.Post("/users", c.Users.Create)
```

### Path Slots

Use slots to capture the value at a specific position in the URL. To define a slot, prefix a path segment with a colon. The slot name must start with a letter, then accept letters, numbers and underscores. Slots can appear anywhere in the path.

Here are a few valid examples:

```go
router.Get("/:id", handler)
router.Get("/v.:version", handler)
router.Get("/:post_id.:format", handler)
router.Get("/:from/:to", handler)
```

The slot values support the URL-safe base64 character set, so they can be any of the following characters:

```
ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_
```

> TODO: fill in a matching and not matching table

### Optional Slots

Optional slots capture zero or one path segments. You can define an optional slot by adding a `?` at the end of the slot. An optional slot must appear at the end of a route.

```go
router.Get("/:id?", handler)
router.Get("/v.:version?", handler)
router.Get("/:post_id.:format?", handler)
router.Get("/:from/:to?", handler)
```

Optional slots treat all characters after the slash and before the colon as optional.

Let's take a look at some examples:

For `/:post_id.:format?`:

| Path      | Match? |
| :-------- | :----: |
| `/1`      |  yes   |
| `/1.json` |  yes   |
| `/10.rss` |  yes   |
| `/1.`     |   no   |
| `/10.`    |   no   |

For `/v.:version?`:

| Path       | Match? |
| :--------- | :----: |
| `/`        |  yes   |
| `/v.1`     |  yes   |
| `/v.1.9.2` |  yes   |
| `/v`       |   no   |
| `/v.`      |   no   |

For `/:from-:to?`:

| Path     | Match? |
| :------- | :----: |
| `/a`     |  yes   |
| `/a-b`   |  yes   |
| `/aa-bb` |  yes   |
| `/aa--`  |  yes   |
| `/a-`    |   no   |

### Wildcard Slots

Optional slots capture zero or more path segments. You can define a wildcard slot by adding a `*` at the end of the slot. A wildcard slot must appear at the end of a route.

```go
router.Get("/:id/:path*", handler)
router.Get("/v.:version*", handler)
```

For `/:id/:path*`:

| Path       | Match? |
| :--------- | :----: |
| `/1`       |  yes   |
| `/1/a`     |  yes   |
| `/1/a/b/c` |  yes   |
| `/`        |   no   |

Wildcard slots treat all characters after the slash and before the colon as optional.

For `/v.:version*`:

| Path     | Match? |
| :------- | :----: |
| `/`      |  yes   |
| `/v.1`   |  yes   |
| `/v.1/a` |  yes   |
| `/v`     |   no   |

### Route Order

The order of routes doesn't matter for the most part. There is no difference between:

```go
router.Get("/", controller.Index)
router.Get("/posts", controller.Posts.Index)
router.Get("/posts/:id", controller.Posts.Show)

// Or
router.Get("/posts/:id", controller.Posts.Show)
router.Get("/posts", controller.Posts.Index)
router.Get("/", controller.Index)

// Or
router.Get("/posts", controller.Posts.Index)
router.Get("/", controller.Index)
router.Get("/posts/:id", controller.Posts.Show)
```

Under the hood, the router splits each path segment into a tree.

```
TODO: MONODRAW TRIE
```

### Route Overlap and Priority

There are some cases where you want route overlap. Consider the following:

```go
router.Get("/explore", controller.Explore)
router.Get("/:user", controller.Users.Show)
```

If you go to `/explore`, both routes match. `/explore` and `/:user`, where `:user` is `explore`. Intuitively, we know what should happen. `/explore` should have higher priority than `/:user`.

Duo uses priorities to resolve these conflicts:

1. Path Constant _(highest priority)_
1. Path Slot
1. Optional Slot
1. Wildcard Slot _(lowest priority)_

The priority is inversely proportional to the number of the matching paths. The smaller number of matching paths, the more priority a route has.

If a route is fully overlapped by other routes, then the router will panic and the server will exit abruptly. This is to avoid routing problems reaching production.

> TODO: Should we panic or Validate? TODO: ?

### Case-Insensitive Routing and Matching

Routing is case-insensitive. The following routes are equivalent:

```go
router.Get("/explore", handler)
router.Get("/Explore", handler)
router.Get("/EXPLORE", handler)
router.Get("/EXpLoRe", handler)
```

If the router encounters equivalent routes, it will panic and the server will exit abruptly. This is to avoid routing problems reaching production.

> TODO: Should we panic or Validate?

Path matching is also case-insensitive. If you type in `/SpecialOffer`, the router will automatically redirect you to `/specialoffer`.

| Path             | Match? |
| :--------------- | :----: |
| `/specialoffer`  |  yes   |
| `/SPECIALOFFER`  |  yes   |
| `/SpecialOffer`  |  yes   |
| `/Special_Offer` |   no   |
| `/SpecialOffer1` |   no   |

### Trailing Slash Trim

> TODO: Trailing Slash Trim

### Disable Default Routing

You may prefer to turn off default routing altogether. You can do this by changing `controller.Router` to the blank `router.Router`:

```go
package web

func Handler(r router.Router, c controller.Directory) http.Handler {

}
```

### Handling the Request from Routing

In rare cases, you may want to serve directly from the router. The router accepts the standard `http.Handler` interface so this straightforward to do:

```go
package web

func Handler(r router.Router, c controller.Directory) http.Handler {
  r.Get("/", c.Index)
  r.Get("/hi", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Hi world!"))
  })
}
```

### Route-Specific Middleware

> TODO: Route-Specific Middleware
