package middleware

import (
	"net/http"
)

// CleanPath middleware
func CleanPath() Middleware {
	return Function(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			next.ServeHTTP(w, r)
		})
	})
}
