// Package httpwrap is a thin wrapper around the wonderful httpsnoop by
// Felix Geisendörfer to make the API more ergonomic for Duo's use case of
// catching WriteHeader and Write to allow for middleware to safely set
// HTTP headers before writing the response.
//
// Unfortunately by wrapping the response, we no longer have streaming writes.
//
package httpwrap

import (
	"bytes"
	"net/http"

	"github.com/felixge/httpsnoop"
)

// Wrap the response writer
func Wrap(w http.ResponseWriter) *ResponseWriter {
	state := new(state)
	responseWriter := httpsnoop.Wrap(w, httpsnoop.Hooks{
		WriteHeader: func(_ httpsnoop.WriteHeaderFunc) httpsnoop.WriteHeaderFunc {
			return func(code int) {
				state.Code = code
			}
		},
		Write: func(_ httpsnoop.WriteFunc) httpsnoop.WriteFunc {
			return func(p []byte) (int, error) {
				return state.Body.Write(p)
			}
		},
	})
	return &ResponseWriter{
		responseWriter,
		state,
		w,
	}
}

// Unwrap the response writer
func Unwrap(w http.ResponseWriter) (rw *ResponseWriter, ok bool) {
	rw, ok = w.(*ResponseWriter)
	return rw, ok
}

// state struct
type state struct {
	Code int
	Body bytes.Buffer
}

// ResponseWriter struct
type ResponseWriter struct {
	http.ResponseWriter
	*state

	original http.ResponseWriter
}

// Flush the response
func (rw *ResponseWriter) Flush() (int, error) {
	if rw.state.Code == 0 {
		rw.state.Code = 200
	}
	rw.original.(http.ResponseWriter).WriteHeader(rw.state.Code)
	return rw.original.(http.ResponseWriter).Write(rw.state.Body.Bytes())
}
