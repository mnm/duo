package middleware

import (
	"net/http"
	"strings"
)

// MethodOverride middleware
func MethodOverride() Middleware {
	return Function(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method != http.MethodPost {
				next.ServeHTTP(w, r)
				return
			}
			switch strings.ToUpper(r.PostFormValue("_method")) {
			case http.MethodPatch:
				r.Method = http.MethodPatch
			case http.MethodDelete:
				r.Method = http.MethodDelete
			}
			next.ServeHTTP(w, r)
		})
	})
}
