package glob

import (
	"path/filepath"
	"strings"

	"github.com/gobwas/glob/syntax/lexer"
)

var sep = string(filepath.Separator)

// Base gets the non-magical part of the glob. This is meant to help reduce the
// search space of the file walker. Similar to gulp's implementation.
func Base(str string) string {
	parts := strings.Split(str, sep)
	var base []string
outer:
	for _, part := range parts {
		lex := lexer.NewLexer(part)
	inner:
		for {
			token := lex.Next()
			switch token.Type {
			case lexer.Text:
				continue
			case lexer.EOF:
				break inner
			default:
				break outer
			}
		}
		base = append(base, part)
	}
	if len(base) == 0 {
		return "."
	} else if len(base) == 1 && base[0] == "" {
		return sep
	}
	return filepath.Clean(strings.Join(base, sep))
}
