# Logging

Logging is a fundamental tool for understanding what your program is doing. You can use any logging system with Duo.

Here's an example of injecting a logger into a show user action:

```go
package users

import (
  "myapp.com/log"
)

type Controller struct {
  Log log.Log
}

func (c *Controller) Show(id int) {
  c.Log.Info("showing user", "id", id)
}
```

## Customizing the Log Handler

By default, all logging is pretty printed to `stderr`. You can change this by creating a new logger in your application's `log` package.

```
myapp
└── log
    └── log.go
```

Inside the `log` package, you can create a `Log` function that accepts a `*log.Entry`:

```go
package log

import (
  "gitlab.com/mnm/duo/log"
)

type Log struct {

}

// TODO: finish this implementation
func (l *Log) Handler(log *log.Entry) error {

}

// Other variation
// TODO: finish this implementation
func (l *Log) Handler(log *log.Entry) {

}

// Other variation
// TODO: finish this implementation
func Handler(log *log.Entry) error {
}

// Other variation
// TODO: finish this implementation
func Handler(log *log.Entry) {

}
```

---

# Notes

---

## Log Levels

The logger supports a couple different levels.

| Level | Description |
| --: | :-- |
| Critical | A major system is not working. Ring the phones. Use sparingly. |
| Error | Something isn't working properly. Investigate soon. |
| Warn | Unexpected behavior has occurred. Might be a problem. Warnings are transitional and should eventually become either Error or Info. |
| Info | Informational message. Provides confidence that the system is working as expected. No action required. |
| Debug | Diagnostic information. Usually turned on temporarily to understand and fix a problem. |

## Optional Key-Value Arguments

The logger accepts a list of optional key-value arguments after the message:

```go
log.Info(message, key1, value1, key2, value2, ...)
```

> **Note:** This API is not fully type-safe. In the future, we'll look into using static analysis to ensure that every key is a string and has a value. This analysis is conceptually similar to how `go vet` warns about `fmt.Sprintf("%s", anInteger)`.

## Creating a Child Logger

You can create child loggers using the `With` function:

```go
package users

import (
  "myapp.com/log"
)

type Controller struct {
  Log log.Log
}

func (c *Controller) Show(id int) {
  log := c.Log.With("action", "users.Show", "path", "GET /users/:id")
  log.Info("showing user", "id", id)
}
```

## Customizing the Log Handler

By default, all logging is pretty printed to `stderr`. You can change this by creating a new logger in your application's `log` package.

```
myapp
└── log
    └── log.go
```

Inside the `log` package, create a `New` function that returns a `log.Log` interface.

```go
package log

import (
  "gitlab.com/mnm/duo/log"
)

func New() log.Log {
  return log.New(console.New(os.Stderr))
}
```

The `console` package is the default log handler. In production, you may prefer to use [logfmt](https://brandur.org/logfmt) to make your logs more `grep`-able.

```go
package log

import (
  "myapp.com/env"
  "gitlab.com/mnm/duo/log"
  "gitlab.com/mnm/duo/log/logfmt"
)

func New(env *env.Env) log.Log {
  // env can use something like: https://github.com/caarlos0/env
  if env.Log.Type == "logfmt" {
    return log.New(logfmt.New(os.Stderr))
  }
  return log.Default()
}
```

> TODO Consider Handler hook instead. (See: hack/log-handler)
