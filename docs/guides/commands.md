# Commands

Tree-based just like files.

```
command
├── serve
│   └── serve.go
└── sleep
    ├── sleep.go
    └── then
        └── then.go
```

## Simplest

```go
package then

import "fmt"

func Run() {
	fmt.Println("running then")
}
```

## More

```go
package sleep

import (
	"context"
	"fmt"
	"time"

	command "github.com/matthewmueller/dev/cli"
)

var Examples = command.Examples{
	{
		Comment: "This is a comment",
		Example: "This is an example",
	},
}

func Run(ctx context.Context, sleep time.Duration) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	case <-time.After(sleep):
		fmt.Println("done sleeping")
		return nil
	}
}
```

## Full

```go
package serve

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4"
	command "github.com/matthewmueller/dev/cli"
)

type Command struct {
	DB *pgx.Conn // injected

	// Anything that doesn't have flag is injected
	Timeout    time.Duration `flag:"timeout in seconds" default:"10s" short:"t"`
	AddressURL string        `flag:"address url to bind to"`
}

// Examples look like this if convenient, otherwise it can be part of the function
var Examples = command.Examples{
	{
		Comment: "This is a comment",
		Example: "This is an example",
	},
}

// Port is a positional argument
func (cmd *Command) Run(ctx context.Context, port int) error {
	fmt.Printf("listening on localhost:%d\n", port)
	return nil
}
```
