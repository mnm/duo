## Who is Behind Duo?

> TODO talk about me

I've been building the pieces of Duo for a long time. Around 9 years actually.

- [Thimble](https://www.youtube.com/watch?v=hZJMR1Kd-9c) _(2012)_: Thimble was Express middleware for bundling your frontend. Supported the latest and greatest languages like Coffeescript and Mustache.
- [DuoJS](https://github.com/duojs/duo) _(2014)_: The original Duo. A package manager and bundler for your frontend.
- [Graph.ql](https://github.com/matthewmueller/graph.ql) _(2015)_: Generate a GraphQL server from a GraphQL schema.
- [Pogo](https://github.com/matthewmueller/pogo) _(2016)_: Introspect your Postgres or SQLite database and generate a type-safe Go client. I've used this tool in every project since 2016. Originally inspired by [xo](https://github.com/xo/xo) and led me to joining Prisma.
- [Elmo](https://elmo-www.netlify.app/) _(2017)_: 4kb Next.js alternative with an emphasis on with pre-rendering and client-side loading for static websites.
- [Joy](https://mat.tm/joy/) _(2017)_: Minimal Go to Javascript compiler. Many of the techniques developed in Joy have been applied to Duo.
- [GqlSql](https://github.com/matthewmueller/gqlsql) _(2017)_: A GraphQL to SQL query compiler. This prototype may find it's way into Duo's models.
- [Koi](https://github.com/matthewmueller/koi) _(2018)_: Turns HTTP handlers into high-level RPC methods. This project directly influenced Duo's controller design.
- [Gumbo](https://github.com/matthewmueller/gumbo) _(2018)_: Another attampt at generate a GraphQL server from a GraphQL schema. This time at build-time and in Go. This work influenced the design of the request body parser and validator. This project may also serve as the backbone for GraphQL support in the future.
- [Stash](https://github.com/matthewmueller/stash) _(2019)_: Encrypted environments. Store your secrets safely in source control. Stash will serve as one of many ways to store environments in Duo.

During this time, I followed the Unix philosophy of building single purpose tools that **do just one thing, but do it well**.

I still value this philosophy, but over the years I realized that I use the same packages in the same way for every project. They live in the same folders and are initialized the same way. I had already built a custom framework. I just carried it from app to app.

And my application is responsible for gluing them together. This glue is mostly the same across the projects too. This was undifferentiated code cluttering up my application.

You may have noticed a pattern in these projects. Run-time to build-time.

After watching Jeffrey Way's Laravel videos, I realized that if I just assume this structure, I can make a lot more assumptions about my application.

It wasn't until I watched some Laravel videos that I realized if I put all these independent packages together, I'd have a framework. And if the modules are expected and in a known location, you could make additional assumptions and code even less.

I've also watched the Rails 1 video on repeat for the last year trying to get Duo to feel the same way with a compiled language like Go. I'm not surprised that my fellow Node developers are having trouble building "the Rails for Node". There's so much depth to that framework even in the early days. It's seriously impressive.

We're not even close yet but the cauldron of the spice of Laravel and Rails with the constraints of Go was how Duo was born.

### Why not Node.JS?

I was a Node developer for about 8 years. Most modern full-stack frameworks are written in Node.js these days. It's reasonable to ask why I chose Go for Duo.

The main reason is because there's a better language for building web servers. It's called Go.

Before you rage-quit,

Setting aside the single-threaded nature of Node

I wrote about this before

### Why not Laravel, Rails or Django?

While I'm awe-stricken by the developers of all these frameworks, frankly after working with Node for awhile and then landing on Go, it's hard to get me excited about any scripting language these days.

While I don't think compiled languages solve all your problems, the low-level access when you need it, the type-safety for maintainence and the single-binary for deployment make compiled languages an easy choice for me these days.

I also don't find Go that much slower to develop in than Node.js or PHP. It's certainly faster to produce code in a script language but the lack of tooling and confusing errors need to be accounted for too. I think Go's extensive standard library and unified toolchain Go make up this time.
