# Say Hello to Duo

<!-- Duo Image -->

Duo is a web framework for prolific developers.

<!-- TODO: need more 🥩 about what Duo can do for you -->

Duo's success is measured in 3 ways:

1. Duo meaningfully reduces your time from idea to market.
2. Smooth scaling from a single developer to many developers. Serves digital nomads and Fortune 500 development teams.
3. Build any web service you want.

## How Duo Works

At its core, Duo is a **Web Framework Compiler**. Duo parses Go files, JS files, CSS files, etc. and generates optimized Go code. The Go compiler takes this optimized Go code and turns it into a self-contained, deployable binary.

<!-- Diagram of files going into Duo, then compiling to Go code -->

The Duo CLI wraps this multi-step process in a few commands to make compiling projects quick and easy.

Just like the Babel compiler or Clang, Duo's compiler is pluggable. Duo currently ships with the following plugins:

- **Request** validates incoming requests.
- **Controller** turns functions into RESTful HTTP handlers.
- **Middleware** plugs into the web server's request-response lifecycle.
- **Session** defines the user data that's persistent across HTTP requests.
- **Log** centralizes framework and app logging. Ship your logs anywhere.

The number of plugins will grow over time. Imagine the possibilities:

<!-- TODO: Some of these are specific to the runtime -->

- **View** understands how to transpile and serve templates, CSS and JSX files.
- **Public** minifies and serves assets from the public/ directory.
- **Trace**
- **Env** provides environment management and secret encryption.
- **Database** read and write to your SQL database.
- **Queue** enqueues jobs for later processing.
- **Schedule** tasks to be run at a certain time of day.
- **Script** runs one-off tasks.
- **Storage** uploads and downloads files.
- **Mail** craft and send emails.
- **Docs** generates documentation for your API.
- **HTTP Client** built just for your web application
- **GraphQL** server could be generated from the controllers.
- **Channel**
- **Admin Dashboard** built just for your web application

Of course, you'll be able to build your own compiler plugins too. This is _actually_ Day 1 folks.

<!-- Signup for Updates -->

## Developer Productivity or Performance... Why not Both?

<!-- Why not both picture -->

The main reason to go to the trouble of hand-rolling a framework compiler is that can Duo offer you a high-level, productive environment for building web applications AND be able to compile down to low-level code with minimal allocations.

If you're ready to get your mind blown by what the most advanced compilers are capable of, I highly recommend https://www.youtube.com/watch?v=zBkNBP00wJE. Neither Duo nor the Go compiler are anywhere close to this and will probably never be because of the required language-level features, but it should still give you an idea of the lower bounds of compiler optimization when you have a team of people optimizing a compiler over many years. Go is doing the same with their compiler. Duo will be able to optimize your web application for you.

<!-- Another avenue worth looking into is Go's SSA optimizations that are already available. -->

This is the reason

Aside from the performance gains which haven't been fully

## Don't Pre-Pay for Framework Features... Pay as you Go.

## Defaults for Everything

In fact, you won't find a configuration files anywhere. The compiler plugins ship with sensible defaults that you can override by defining your own provider in the code itself.

## Escape Hatches Everywhere

## Wired together with a Hint of Lime... I mean Dependency Injection

## Strong leaning towards Type-Safety and Compile-Time Code Generation

## Shared Volcabulary

That allow developers to hop in and out of Duo applications.

## Standard Interfaces

Queues enqueue and dequeue. Storager downloads and uploads. Mailers email. Renderers render. Loggers log.

Duo standardizes many of these interfaces allowing you to swap out the underlying implementation.

## One Binary, Many Programs

This will become more apparent as time goes on but the final binary actually contains many different programs. In addition to being a web server, it can also be a worker that processes enqueued jobs, it can run one-off tasks or listen for scheduled events.

You can choose between deploying the binary as a majestic monolith or deploying this binary many times across microservices. At the end of the day, it's still just one binary.

## Start with the Basics and Move Forward

## Is Duo Production-Ready?

Not yet! The code is well-tested, but not battle-hardened. I'm sure you'll discover lots of little quirks. These are early days.

Right now Duo should be enough for most API servers and event-driven servers where you respond to webhooks. You can also render HTML and write to a database, but it's more tedious than it should be.

## Where's it headed?

First ship Duo 0.1 and focus on what early supporters need. In the background, progress will be made in the following areas:

- Views: React, V8, Preact, Svelte
  - esbuild, rusty-v8 bindings,
- Models, Migrations and Database access
  - Postgres, MySQL, SQLite
- High-Order features
  - Queues: Memory, SQS
  - Mail: IMAP, Mailgun, SES
  - Storage: S3, ...
  - Channels: Pusher, ...
  - ... etc. This is mostly driven by you

## FAQ

<!-- This can probably go on the pricing page -->

### Why not Node.js?

"Go seems like an odd choice for a web framework".

I'm old enough to remember a time when all the web tooling was written in Ruby. Being an early adopter in the Node.js community, I helped bring a lot of the developer tooling over to the Node.js ecosystem.

I've always enjoyed writing Node.js but I found it difficult to maintain growing Node applications in production. I think this is quite bit better now with Typescript, but

As a student of programming history, I like to reminisce on the original goal of things.

Node's shifting language, modules, community places a needlessly high burden on maintainers. Fragmented ecosystem. Single threaded. Too many changes to keep up with

Go is a comparably calmer ecosystem. There was a major shift to Go Modules, and ther will be some waves once Generics are introduced in a couple months, but for the last 10 years the language the language hasn't changed much.

This will be the case for Duo as well. Once we reach Duo 1.0, we don't plan to make anymore breaking API changes.

First and foremost, Go was primarily built for large engineering teams building mammoth web services. Go also happens to be simple and scales down to solo developers launching products. It's built for concurrency. It shows in the standard library.

I believe Go lacks higher-level entrypoints into the ecosystem. I hope to provide that with Duo.

### Why charge for Duo?

Because I want you, dear developer, to be my sole focus professionally and my boss. I like working with my own kind so I'm coming to you, directly.

I've been working on developer tools for the last 10 years in my spare time and professionally. Duo brings together everything that I've learned and worked on into a single cohesive package. There's still so much uncharted space that needs exploring. I'd love to spend the next 10 years working for you on developer tooling.

This plan works best if Duo itself is self-sustainable. Then I don't need to upsell you on a platform, write books or beg you for donations. My sole focus can be on the framework and I can work with partners in the ecosystem to make Duo work great with everyone.

I can hire designers to spruce up the experience, developers to ship new features faster and customer support to troubleshoot with you. The frameworks of yesterday have all been side-gigs to promote the main attraction. I want the Duo framework to be the main attraction.

- More focused on developing features for people
- People are financially invested in the success of the framework
- Allows us to focus on developing features.
- Allows us to hire help
- Better support for customers
