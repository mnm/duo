package virtual

import (
	"io"
	"io/fs"
	"net/http"
	"time"
)

func File(name string, data []byte) http.File {
	return &file{
		Path:    name,
		Data:    data,
		ModTime: time.Now(),
		Mode:    0644,
	}
}

type file struct {
	Path    string
	ModTime time.Time
	Data    []byte

	// Optional
	Mode fs.FileMode
	Sys  interface{}

	// Seek support
	offset int64
}

var _ fs.File = (*file)(nil)

func (f *file) Read(b []byte) (int, error) {
	if f.offset >= int64(len(f.Data)) {
		f.offset = 0 // Reset for next read
		return 0, io.EOF
	}
	if f.offset < 0 {
		return 0, &fs.PathError{Op: "read", Path: f.Path, Err: fs.ErrInvalid}
	}
	n := copy(b, f.Data[f.offset:])
	f.offset += int64(n)
	return n, nil
}

// Seek support for http.File
func (f *file) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	case 0:
		// offset += 0
	case 1:
		offset += f.offset
	case 2:
		offset += int64(len(f.Data))
	}
	if offset < 0 || offset > int64(len(f.Data)) {
		return 0, &fs.PathError{Op: "seek", Path: f.Path, Err: fs.ErrInvalid}
	}
	f.offset = offset
	return offset, nil
}

// Files aren't directories
func (f *file) Readdir(count int) ([]fs.FileInfo, error) {
	return []fs.FileInfo{}, nil
}

func (f *file) Close() error {
	return nil
}

func (f *file) Stat() (fs.FileInfo, error) {
	return &fi{f}, nil
}

var _ fs.DirEntry = (*file)(nil)

func (f *file) Name() string {
	return f.Path
}

func (f *file) Info() (fs.FileInfo, error) {
	return &fi{f}, nil
}

func (f *file) IsDir() bool {
	return false
}

func (f *file) Type() fs.FileMode {
	if f.Mode == 0 {
		f.Mode = 0644
	}
	return f.Mode
}

type fi struct {
	f *file
}

func (i *fi) Name() string       { return i.f.Path }
func (i *fi) Size() int64        { return int64(len(i.f.Data)) }
func (i *fi) Mode() fs.FileMode  { return i.f.Type() }
func (i *fi) ModTime() time.Time { return i.f.ModTime }
func (i *fi) IsDir() bool        { return false }
func (i *fi) Sys() interface{}   { return i.f.Sys }
