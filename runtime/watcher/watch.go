package watcher

import (
	"context"
	"io/fs"
	"os"
	"path/filepath"

	"github.com/fsnotify/fsnotify"
)

// Watch function
func Watch(ctx context.Context, dir string, fn func(path string) error) error {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	defer watcher.Close()
	gitignore, err := newGitIgnore(filepath.Join(dir, ".gitignore"))
	if err != nil {
		return err
	}
	walker := func(path string, de fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		isDir := de.IsDir()
		if gitignore.Match(path, isDir) || filepath.Base(path) == ".git" {
			if isDir {
				return filepath.SkipDir
			}
			return nil
		}
		watcher.Add(path)
		return nil
	}
	if err := filepath.WalkDir(dir, walker); err != nil {
		return err
	}
	// watch for file events!
	for {
		select {
		case <-ctx.Done():
			return nil
		case err := <-watcher.Errors:
			return err
		case evt := <-watcher.Events:
			switch evt.Op {
			// Ignore "CHMOD" events.
			case fsnotify.Chmod:
				continue
			// Remove the file or directory from the watcher.
			// We intentionally ignore errors for this case.
			case fsnotify.Remove:
				watcher.Remove(evt.Name)
				continue
			// Try watching a the file as long as it's not inside .gitignore.
			// Ignore most errors since missing a file isn't the end of the world.
			case fsnotify.Create:
				fi, err := os.Stat(evt.Name)
				if err != nil {
					continue
				}
				if gitignore.Match(evt.Name, fi.IsDir()) {
					continue
				}
				err = watcher.Add(evt.Name)
				if err != nil {
					return err
				}
				continue
			// A file has been updated. Notify our matchers.
			case fsnotify.Write:
				// Run the handler
				if err := fn(evt.Name); err != nil {
					return err
				}
			}
		}
	}
}
