# Markdown Pipeline

Rendered with [Goldmark](https://github.com/yuin/goldmark). Look to [Hugo](https://github.com/gohugoio/hugo/tree/master/markup/goldmark) for some examples of writing plugins.

- Callouts
- [Table of Contents](https://github.com/gohugoio/hugo/blob/master/markup/goldmark/toc.go)
- [Auto-linking headers](https://github.com/yuin/goldmark#built-in-extensions)
- [Syntax highlighting](https://github.com/yuin/goldmark-highlighting)
- Next & Previous links
