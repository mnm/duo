package middleware

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/mnm/duo/session"
)

// Session context key
type sessionContext struct{}

// Session middleware creates session storage middleware
func Session(store session.Store) Middleware {
	return Function(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Read the session from session storage
			ses, err := store.Read(r)
			if err != nil {
				fmt.Printf("Session middleware error: %v\n", err)
				return
			}
			// TODO: Log that we found or created the session with the session ID
			// Refresh the session
			session.Refresh(ses, r)
			// Store the session in context
			r = r.WithContext(context.WithValue(r.Context(), sessionContext{}, ses))
			// Next middleware
			next.ServeHTTP(w, r)
			// Write an updated session to session storage
			err = store.Write(w, ses)
			if err != nil {
				fmt.Printf("Session write error: %v\n", err)
				return
			}
		})
	})
}

// SessionFrom fn
func SessionFrom(ctx context.Context) *session.Session {
	if session, ok := ctx.Value(sessionContext{}).(*session.Session); ok {
		return session
	}
	// TODO: empty session
	return nil
}
