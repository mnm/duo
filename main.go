package main

import (
	"fmt"
	"net/http"
)

func main() {
	fn := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.URL.Path)
	})
	http.ListenAndServe(":8000", fn)
}
