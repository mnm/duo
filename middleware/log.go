package middleware

import (
	"context"
	"net/http"

	"github.com/segmentio/ksuid"
	"gitlab.com/mnm/duo/log"
)

// Log context key
type logContext struct{}

// Log middleware
func Log(log log.Log) Middleware {
	return Function(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			id, err := ksuid.NewRandom()
			if err != nil {
				log.Error("Unable to initialize request id. %v", err)
				// TODO: Not sure what to do here. It happens so early in the stack...
				w.WriteHeader(500)
				w.Write([]byte(`An unexpected error occurred.`))
				return
			}
			log := log.Field("request_id", id.String())
			r = r.WithContext(context.WithValue(r.Context(), logContext{}, log))
			next.ServeHTTP(w, r)
		})
	})
}

// LogFrom retrieves the request-scoped logger from the context
func LogFrom(ctx context.Context) log.Log {
	if log, ok := ctx.Value(logContext{}).(log.Log); ok {
		return log
	}
	// TODO: discard logger
	return nil
}
