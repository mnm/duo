package watcher

import (
	"os"
	"path/filepath"

	"github.com/monochromegane/go-gitignore"
)

func newGitIgnore(path string) (*gitIgnore, error) {
	gi := &gitIgnore{path: path}
	if err := gi.tryLoad(); err != nil {
		return nil, err
	}
	return gi, nil
}

type gitIgnore struct {
	path string

	// inner may be nil
	inner gitignore.IgnoreMatcher
}

var _ gitignore.IgnoreMatcher = (*gitIgnore)(nil)

func (g *gitIgnore) tryLoad() error {
	dir := filepath.Dir(g.path)
	file, err := os.Open(g.path)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	defer file.Close()
	g.inner = gitignore.NewGitIgnoreFromReader(dir, file)
	return nil
}

func (g *gitIgnore) Match(path string, isDir bool) bool {
	if g.inner == nil {
		return false
	}
	return g.inner.Match(path, isDir)
}
