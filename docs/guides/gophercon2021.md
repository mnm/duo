# Powering the Next Rails in Go

Go has a reputation of being a verbose but predictable language, best used for building micro-services in large companies.

Let's change this perception. What if Go was the premiere "get stuff done" language for web developers?

I'm here to talk about how I built a web framework compiler for Go.

---

I believe that underneath Go's rough exterior lies a beautifully, simple and straightforward language.

For the last year, I've been obsessed with trying to make Go a good fit for web developers. I've been trying to answer a simple question: How could we make Go feel like Rails without giving up what we love about Go? This talk is about what I came up with and the discoveries I made along the way.

Why does it matter? Because I love building web applications, I love writing Go and I'm tired of working in the Javascript ecosystem with its ever-increasing complexity. I want web development to be fun and productive again.

Let's break our question down into 2 parts:

1. How could we make Go feel like Rails?

   If you squint, Go and Rails share a similar philosophy of convention over configuration. You see this philosophy in how Go differentiates between private and public methods with a capital letter.

   Rails takes this farther. To define a route to create a user, you can do the following:

   ```ruby
   def create
     @user = User.new(params[:user])
   end
   ```

   With this simple one-liner, we've defined the `POST /users` route, initialized a User model and bound that user model to our view. Now that's some heavy lifting for one line of code!

   As Gophers, we're used to doing something like this:

   ```go
   router.Post("/users", func(w http.ResponseWriter, r *http.Request) {
     body, err := ioutil.ReadAll(r.Body)
     var user User
     err := json.Unmarshal(body, &user)
     view, err := template.Generate("users/create", user)
     w.Write(view)
   })
   ```

   Not so bad. Actually if you ignore the error handling, it's quite straightforward. Let's step back from what we know for a second and just think about the ideal API. If you give it a moment, I think you'd come up with something like this:

   ```go
   func Create(input *Input) (output *User, err error) {
   }
   ```

   Much better right? I want to write Go like this! So how can we translate our ideal into what's needed? One possibility is to use the reflect API. While, you can use reflection to get the ideal API above, you lose type-safety. This gets me to my second design constraint.

2. ... without giving up what we love about Go?

   One way to get Rails is to have APIs with empty interfaces everywhere. But this ruins one of the best parts of Go: its straightforward and performant type-system.

   We also cherish the predictability of Go's scoping rules. Magic variables don't appear in scope out of nowhere. Rails uses global variables in many places for convenience. Rails can get away with this because of its execution model. With Go, we don't have nor want this luxury. We'll use dependency injection to achieve the convenience without resorting to global variables.

So this has been my quest: bringing the convenience of Rails to the Go ecosystem.

During the last year of lockdown, I've been working on what I call a web framework compiler called Duo. Duo is a full-stack web framework written for Go that makes building web applications much easier. Duo's design optimizes for 3 parts of your applications life-cycle:

- Launch faster by writing radically Less Code
- Maintain over the long-haul with type-safety and compiler guarantees
- Straightforward deploys with a single web application binary

Let's see how.

Programming languages like Go are compiled from high-level languages with loops and conditionals to low-level Assembly instructions like adds and jumps.

Duo moves up a layer. Duo is a compiler for your entire web application. It takes your project as input and outputs a web application binary. You can write the high-level application code and let Duo compile, optimize and generate low-level server code.

Duo's compiler gives you the productivity gains of working at a high-level without the tradeoffs in runtime performance.

---

Here's a basic outline of the talk. The order may change slightly over time, but the components won't move too much:

1. Introduce and explain why Duo (what I've written about already)
2. Demo Duo (controllers and views). Inspired by one of the first Rails talks: https://www.youtube.com/watch?v=Gzj723LkRJY
3. Share how Duo was implemented.
   - High-level, performant Go parser built on top of go/parser
   - Build-system that is powered by plugins and a DAG (imagine a programmatic Makefile)
   - A stack of compiler plugins that parse Go code and generate controllers, views, web server, etc.
   - Dependency injection system to glue the packages together
   - Good ole' `go build` to build the single binary at the end.

I would also like GopherCon 2021 to be my first public talk introducing Duo to the world. I can also talk about using gov8 and esbuild to implement React-compatible views if there's time/interest.

Why I might be a good fit? I built and released a Javascript to Go compiler a couple years back that uses a lot of the same techniques. I've also been building many of the components of this framework for a long time. Just this last year, I started bringing them together.
