package middleware

import (
	"fmt"
	"net/http"
	"os"
	"path"
)

// Public serves from the public folder
func Public(fs http.FileSystem) Middleware {
	return Function(func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			urlPath := r.URL.Path
			if r.Method != http.MethodGet || path.Ext(urlPath) == "" {
				next.ServeHTTP(w, r)
				return
			}
			file, err := fs.Open(urlPath)
			if err != nil {
				if os.IsNotExist(err) {
					next.ServeHTTP(w, r)
					return
				}
				fmt.Println("Static open error", err)
				return
			}
			stat, err := file.Stat()
			if err != nil {
				fmt.Println("Static stat error", err)
				return
			}
			if stat.IsDir() {
				next.ServeHTTP(w, r)
				return
			}
			// All public/ data is currently gzipped. It appears that all browsers
			// support gzip encoding, so we'll always send the gzipped version down.
			// If that turns out not to be the case, we should adjust this. The Vary
			// response header is recommended by MDN as a way to improve caching.
			header := w.Header()
			header.Add("Content-Encoding", "gzip")
			header.Add("Vary", "Accept-Encoding")
			http.ServeContent(w, r, urlPath, stat.ModTime(), file)
		})
	})
}
