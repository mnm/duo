package glob_test

import (
	"testing"

	"github.com/matryer/is"
	"gitlab.com/mnm/duo/intern/glob"
)

func TestBase(t *testing.T) {
	is := is.New(t)
	for _, test := range bases {
		t.Run(test.input, func(t *testing.T) {
			base := glob.Base(test.input)
			is.Equal(test.expected, base)
		})
	}
}

var bases = []struct {
	input    string
	expected string
}{
	// should strip glob magic to return parent path
	{".", "."},
	{".*", "."},
	{"a/*/b", "a"},
	{"a*/.*/b", "."},
	{"*/a/b/c", "."},
	{"*", "."},
	{"*/", "."},
	{"*/*", "."},
	{"*/*/", "."},
	{"**", "."},
	{"**/", "."},
	{"**/*", "."},
	{"**/*/", "."},
	{"/*.js", "/"},
	{"*.js", "."},
	{"**/*.js", "."},
	{"{a,b}", "."},
	{"/{a,b}", "/"},
	{"/{a,b}/", "/"},
	{"{a,b}", "."},
	{"/{a,b}", "/"},
	{"./{a,b}", "."},
	{"path/to/*.js", "path/to"},
	{"/root/path/to/*.js", "/root/path/to"},
	{"chapter/foo [bar]/", "chapter"},
	{"path/[a-z]", "path"},
	{"[a-z]", "."},
	{"path/{to,from}", "path"},
	{"path/?/foo", "path"},
	{"path/*/foo", "path"},
	{"path/**/*", "path"},
	{"path/**/subdir/foo.*", "path"},
	{"path/subdir/**/foo.js", "path/subdir"},
	{"path/{foo,bar}/", "path"},
}
