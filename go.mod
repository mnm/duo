module gitlab.com/mnm/duo

go 1.16

require (
	github.com/ajg/form v1.5.2-0.20200323032839-9aeb3cf462e1
	github.com/armon/go-radix v1.0.0
	github.com/cespare/xxhash v1.1.0
	github.com/evanw/esbuild v0.12.24
	github.com/felixge/httpsnoop v1.0.1
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gitchander/permutation v0.0.0-20201214100618-1f3e7285f953
	github.com/gobwas/glob v0.2.3
	github.com/jackc/puddle v1.1.3
	github.com/matryer/is v1.4.0
	github.com/matthewmueller/diff v0.0.0-20201217171827-576175ea57be
	github.com/matthewmueller/gotext v0.0.0-20210424201144-265ed61725ac
	github.com/matthewmueller/text v0.0.0-20210424201111-ec1e4af8dfe8
	github.com/monochromegane/go-gitignore v0.0.0-20200626010858-205db1a8cc00
	github.com/segmentio/ksuid v1.0.3
	github.com/timewasted/go-accept-headers v0.0.0-20130320203746-c78f304b1b09
	github.com/tj/assert v0.0.3
	github.com/tomasen/realip v0.0.0-20180522021738-f0c99a92ddce
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	rogchap.com/v8go v0.6.0
)
