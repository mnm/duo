package dirsync

import (
	"errors"
	"io/fs"
	"path/filepath"
	"strconv"

	"gitlab.com/mnm/duo/intern/dirsync/set"
	"gitlab.com/mnm/duo/intern/vfs"
)

// TODO: read files during diff so we ensure we're successful before writing.
// TODO: update should compare stamps at the time of writing, not before.

func New(sourceFS fs.FS, targetFS vfs.ReadWritable) *Syncer {
	return &Syncer{sourceFS, targetFS}
}

type Syncer struct {
	sourceFS fs.FS
	targetFS vfs.ReadWritable
}

func (s *Syncer) Sync(sourceDir, targetDir string) error {
	ops, err := s.diff(sourceDir, targetDir)
	if err != nil {
		return err
	}
	return s.apply(ops)
}

type OpType uint8

func (ot OpType) String() string {
	switch ot {
	case CreateType:
		return "create"
	case UpdateType:
		return "update"
	case DeleteType:
		return "delete"
	default:
		return ""
	}
}

const (
	CreateType OpType = iota + 1
	UpdateType
	DeleteType
)

type Op struct {
	Type OpType
	Path string
}

func (o Op) String() string {
	return o.Type.String() + ":" + o.Path
}

func (s *Syncer) diff(sourceDir, targetDir string) (ops []Op, err error) {
	sourceEntries, err := fs.ReadDir(s.sourceFS, sourceDir)
	if err != nil {
		return nil, err
	}
	targetEntries, err := fs.ReadDir(s.targetFS, targetDir)
	if err != nil && !errors.Is(err, fs.ErrNotExist) {
		return nil, err
	}
	sourceSet := set.New(sourceEntries...)
	targetSet := set.New(targetEntries...)
	creates := set.Difference(sourceSet, targetSet)
	deletes := set.Difference(targetSet, sourceSet)
	updates := set.Intersection(sourceSet, targetSet)
	createOps, err := s.createOps(sourceDir, creates.List())
	if err != nil {
		return nil, err
	}
	deleteOps := s.deleteOps(sourceDir, deletes.List())
	childOps, err := s.updateOps(sourceDir, targetDir, updates.List())
	if err != nil {
		return nil, err
	}
	ops = append(ops, createOps...)
	ops = append(ops, deleteOps...)
	ops = append(ops, childOps...)
	return ops, nil
}

func (s *Syncer) createOps(dir string, des []fs.DirEntry) (ops []Op, err error) {
	for _, de := range des {
		path := filepath.Join(dir, de.Name())
		if !de.IsDir() {
			ops = append(ops, Op{CreateType, path})
			continue
		}
		des, err := fs.ReadDir(s.sourceFS, path)
		if err != nil {
			return nil, err
		}
		createOps, err := s.createOps(path, des)
		if err != nil {
			return nil, err
		}
		ops = append(ops, createOps...)
	}
	return ops, nil
}

func (s *Syncer) deleteOps(dir string, des []fs.DirEntry) (ops []Op) {
	for _, de := range des {
		path := filepath.Join(dir, de.Name())
		ops = append(ops, Op{DeleteType, path})
		continue
	}
	return ops
}

func (s *Syncer) updateOps(sourceDir, targetDir string, des []fs.DirEntry) (ops []Op, err error) {
	for _, de := range des {
		sourcePath := filepath.Join(sourceDir, de.Name())
		targetPath := filepath.Join(targetDir, de.Name())
		// Recurse directories
		if de.IsDir() {
			childOps, err := s.diff(sourcePath, targetPath)
			if err != nil {
				return nil, err
			}
			ops = append(ops, childOps...)
			continue
		}
		// Otherwise, check if the file has changed
		sourceStamp, err := stamp(s.sourceFS, sourcePath)
		if err != nil {
			return nil, err
		}
		targetStamp, err := stamp(s.targetFS, targetPath)
		if err != nil {
			return nil, err
		}
		if sourceStamp != targetStamp {
			ops = append(ops, Op{UpdateType, sourcePath})
		}
	}
	return ops, nil
}

func (s *Syncer) apply(ops []Op) error {
	for _, op := range ops {
		switch op.Type {
		case CreateType:
			dir := filepath.Dir(op.Path)
			if err := s.targetFS.MkdirAll(dir, 0755); err != nil {
				return err
			}
			data, err := fs.ReadFile(s.sourceFS, op.Path)
			if err != nil {
				return err
			}
			if err := s.targetFS.WriteFile(op.Path, data, 0644); err != nil {
				return err
			}
		case UpdateType:
			data, err := fs.ReadFile(s.sourceFS, op.Path)
			if err != nil {
				return err
			}
			if err := s.targetFS.WriteFile(op.Path, data, 0644); err != nil {
				return err
			}
		case DeleteType:
			if err := s.targetFS.RemoveAll(op.Path); err != nil {
				return err
			}
		}
	}
	return nil
}

// Stamp the path, returning "" if the file doesn't exist.
// Uses the modtime and size to determine if a file has changed.
func stamp(fsys fs.FS, path string) (stamp string, err error) {
	stat, err := fs.Stat(fsys, path)
	if err != nil {
		if errors.Is(err, fs.ErrNotExist) {
			return "-1:-1", nil
		}
		return "", err
	}
	mtime := stat.ModTime().UnixNano()
	size := stat.Size()
	stamp = strconv.Itoa(int(size)) + ":" + strconv.Itoa(int(mtime))
	return stamp, nil
}
