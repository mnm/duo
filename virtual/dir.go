package virtual

import (
	"io/fs"
	"path"
	"time"
)

// ReadDir lists files from fs. ReadDir assumes that we've already checked that
// the file is a directory. That's why we accept a FileInfo instead of a name.
func ReadDir(fsys fs.FS, fi fs.FileInfo) (fs.File, error) {
	dir := fi.Name()
	des, err := fs.ReadDir(fsys, dir)
	if err != nil {
		return nil, err
	}
	entries := make([]fs.DirEntry, len(des))
	for i, de := range des {
		entries[i] = &dirEntry{
			fsys: fsys,
			dir:  dir,
			name: de.Name(),
			mode: de.Type(),
		}
	}
	return &directory{
		Path:    dir,
		MTime:   fi.ModTime(),
		Entries: entries,
	}, nil
}

func Dir(fsys fs.FS, dir string, files ...string) fs.File {
	entries := make([]fs.DirEntry, len(files))
	for i, file := range files {
		var mode fs.FileMode = 0644
		if path.Ext(file) == "" {
			mode = 0755 | fs.ModeDir
		}
		entries[i] = &dirEntry{
			fsys: fsys,
			name: file,
			dir:  dir,
			mode: mode,
		}
	}
	return &directory{
		Path:    dir,
		MTime:   time.Now(),
		Entries: entries,
	}
}

type dirEntry struct {
	fsys fs.FS
	name string
	dir  string
	mode fs.FileMode
}

var _ fs.DirEntry = (*dirEntry)(nil)

func (de *dirEntry) Name() string {
	return de.name
}

func (de *dirEntry) Info() (fs.FileInfo, error) {
	return fs.Stat(de.fsys, path.Join(de.dir, de.name))
}

func (de *dirEntry) IsDir() bool {
	return de.mode&fs.ModeDir != 0
}

func (de *dirEntry) Type() fs.FileMode {
	return de.mode
}

type directory struct {
	Path    string
	MTime   time.Time
	Entries []fs.DirEntry

	// Optional
	Mode fs.FileMode
	Sys  interface{}
}

var _ fs.File = (*directory)(nil)
var _ fs.ReadDirFile = (*directory)(nil)
var _ fs.DirEntry = (*directory)(nil)

func (d *directory) Name() string {
	return d.Path
}

func (d *directory) Type() fs.FileMode {
	return d.Mode
}

func (d *directory) IsDir() bool {
	return true
}
func (d *directory) Read(b []byte) (int, error) {
	return 0, fs.ErrInvalid
}

func (d *directory) Close() error {
	return nil
}

func (d *directory) Stat() (fs.FileInfo, error) {
	return &di{d}, nil
}

func (d *directory) Info() (fs.FileInfo, error) {
	return &di{d}, nil
}

func (d *directory) ReadDir(n int) ([]fs.DirEntry, error) {
	return d.Entries, nil
}

type di struct {
	d *directory
}

func (i *di) Name() string       { return i.d.Path }
func (i *di) Size() int64        { return 0 }
func (i *di) Mode() fs.FileMode  { return i.d.Type() }
func (i *di) ModTime() time.Time { return i.d.MTime }
func (i *di) IsDir() bool        { return true }
func (i *di) Sys() interface{}   { return i.d.Sys }
