package log

import (
	"container/list"
	"fmt"
	"os"
	"strings"
)

// New logger
func New(loggers ...Logger) Log {
	return &logger{
		loggers: loggers,
		fields:  newFields(),
	}
}

// ParseLevel parses a string into a defined
// log level. If the string is not a
func ParseLevel(s string) (level Level, err error) {
	switch strings.ToLower(s) {
	case "debug":
		return DebugLevel, nil
	case "info":
		return InfoLevel, nil
	case "notice":
		return NoticeLevel, nil
	case "warn":
		return WarnLevel, nil
	case "error":
		return ErrorLevel, nil
	case "fatal":
		return FatalLevel, nil
	default:
		return -1, fmt.Errorf("log: %q is not a valid log level", s)
	}
}

// Fields alias
type Fields = map[string]interface{}

// Log interface
type Log interface {
	Field(key string, value interface{}) Log
	Fields(fields map[string]interface{}) Log
	Debug(message string, args ...interface{})
	Info(message string, args ...interface{})
	Notice(message string, args ...interface{})
	Warn(message string, args ...interface{})
	Error(message string, args ...interface{})
	Fatal(message string, args ...interface{})
}

// Level of logs
type Level int

// Log level
const (
	DebugLevel Level = iota
	InfoLevel
	NoticeLevel
	WarnLevel
	ErrorLevel
	FatalLevel
)

// Input struct
type Input struct {
	Level   Level
	Fields  *fields
	Message string
}

// Logger interface
type Logger interface {
	Log(log Input)
}

// Flusher is an optional interface
type Flusher interface {
	Flush()
}

type logger struct {
	loggers []Logger
	fields  *fields
}

func (l *logger) copy() *logger {
	return &logger{
		fields:  l.fields.copy(),
		loggers: l.loggers,
	}
}

// Fields is a map of key values
// Based on: https://github.com/elliotchance/orderedmap/blob/master/orderedmap.go
type fields struct {
	kv map[string]*list.Element
	ll *list.List
}

type field struct {
	key   string
	value interface{}
}

func newFields() *fields {
	return &fields{
		kv: make(map[string]*list.Element),
		ll: list.New(),
	}
}

func (f *fields) copy() *fields {
	copy := newFields()
	for _, key := range f.Keys() {
		copy.set(key, f.Get(key))
	}
	return copy
}

// set a field
func (f *fields) set(key string, value interface{}) bool {
	_, didExist := f.kv[key]
	if !didExist {
		element := f.ll.PushBack(&field{key, value})
		f.kv[key] = element
	} else {
		f.kv[key].Value.(*field).value = value
	}
	return !didExist
}

// Get a field
func (f *fields) Get(key string) (value interface{}) {
	element, ok := f.kv[key]
	if ok {
		return element.Value.(*field).value
	}
	return nil
}

// Keys gets all the keys in order
func (f *fields) Keys() (keys []string) {
	keys = make([]string, len(f.kv))
	element := f.ll.Front()
	for i := 0; element != nil; i++ {
		keys[i] = element.Value.(*field).key
		element = element.Next()
	}
	return keys
}

func (l *logger) Field(key string, value interface{}) Log {
	log := l.copy()
	log.fields.set(key, value)
	return log
}

func (l *logger) Fields(fields map[string]interface{}) Log {
	log := l.copy()
	for key, value := range fields {
		log.fields.set(key, value)
	}
	return log
}

func (l *logger) Debug(message string, args ...interface{}) {
	msg := fmt.Sprintf(message, args...)
	for _, logger := range l.loggers {
		logger.Log(Input{DebugLevel, l.fields, msg})
	}
}

func (l *logger) Info(message string, args ...interface{}) {
	msg := fmt.Sprintf(message, args...)
	for _, logger := range l.loggers {
		logger.Log(Input{InfoLevel, l.fields, msg})
	}
}

func (l *logger) Notice(message string, args ...interface{}) {
	msg := fmt.Sprintf(message, args...)
	for _, logger := range l.loggers {
		logger.Log(Input{NoticeLevel, l.fields, msg})
	}
}

func (l *logger) Warn(message string, args ...interface{}) {
	msg := fmt.Sprintf(message, args...)
	for _, logger := range l.loggers {
		logger.Log(Input{WarnLevel, l.fields, msg})
	}
}

func (l *logger) Error(message string, args ...interface{}) {
	msg := fmt.Sprintf(message, args...)
	for _, logger := range l.loggers {
		logger.Log(Input{ErrorLevel, l.fields, msg})
	}
}

func (l *logger) Fatal(message string, args ...interface{}) {
	msg := fmt.Sprintf(message, args...)
	for _, logger := range l.loggers {
		logger.Log(Input{FatalLevel, l.fields, msg})
		// Flush the logs, if needed
		if flusher, ok := logger.(Flusher); ok {
			flusher.Flush()
		}
	}
	// Exit with status 1
	os.Exit(1)
}
