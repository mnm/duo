# Reference

## Directory Structure

| Folder     | Description |
| :--------- | :---------- |
| controller |             |
| duo        |             |
| log        |             |
| middleware |             |
| web        |             |

> TODO: finish directory structure table

### Reserved Folders

Reserved for future use.

| Folder | Description |
| :----- | :---------- |
| env    |             |
| view   |             |
| model  |             |
| mail   |             |
| queue  |             |

> TODO: finish reserved folder table

## CLI

| Command                           | Description |
| :-------------------------------- | :---------- |
| `duo new <directory>`             |             |
| `duo run`                         |             |
| `duo add <scaffolder> [args ...]` |             |
| `duo build [args...]`             |             |
| `duo upgrade`                     |             |
| `duo version`                     |             |
| `duo help`                        |             |

You can run `duo help` for more detailed information

> TODO: finish CLI table

## Packages

Duo provides a number of supporting packages that tightly integrate into the framework.

| Import Path                 | Description |
| :-------------------------- | :---------- |
| `gitlab.com/mnm/duo/router` |             |
| `gitlab.com/mnm/duo/log`    |             |

> TODO: finish packages table

### Internal

| Import Path                  | Description |
| :--------------------------- | :---------- |
| `gitlab.com/mnm/duo/request` |             |
| `TODO`                       |             |

> TODO: finish internal packages table
