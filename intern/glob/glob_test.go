package glob_test

import (
	"errors"
	"io/fs"
	"testing"
	"testing/fstest"

	"github.com/matryer/is"
	"gitlab.com/mnm/duo/intern/glob"
)

func TestGlob(t *testing.T) {
	is := is.New(t)
	matcher, err := glob.Compile("view/{**,*}.{svelte,jsx}")
	is.NoErr(err)
	is.True(matcher.Match("view/index.svelte"))
	is.True(matcher.Match("view/about/about.svelte"))
	is.True(matcher.Match("view/users/posts/comments.svelte"))
	is.True(matcher.Match("view/index.jsx"))
	is.True(matcher.Match("view/about/about.jsx"))
	is.Equal(false, matcher.Match("view/index.go"))
	is.Equal(false, matcher.Match("view/about/about.go"))
}

func TestNested(t *testing.T) {
	is := is.New(t)
	fs := fstest.MapFS{
		"generated/hello.go": &fstest.MapFile{
			Data: []byte("hello"),
		},
		"generated/cool.go": &fstest.MapFile{
			Data: []byte("cool"),
		},
		"generated/yello.go": &fstest.MapFile{
			Data: []byte("yello"),
		},
	}
	files, err := glob.Match(fs, "generated/*ello.go")
	is.NoErr(err)
	is.Equal(len(files), 2)
	is.Equal(files[0], "generated/hello.go")
	is.Equal(files[1], "generated/yello.go")
}
func TestWildcard(t *testing.T) {
	is := is.New(t)
	fs := fstest.MapFS{
		"generated/controller/controller.go": &fstest.MapFile{
			Data: []byte("hello"),
		},
	}
	files, err := glob.Match(fs, "generated/controller/**/controller.go")
	is.NoErr(err)
	is.Equal(len(files), 1)
	is.Equal(files[0], "generated/controller/controller.go")
}

func TestNoMatch(t *testing.T) {
	is := is.New(t)
	fsys := fstest.MapFS{
		"generated/controller/controller.go": &fstest.MapFile{
			Data: []byte("hello"),
		},
	}
	files, err := glob.Match(fsys, "hello.go")
	is.True(errors.Is(err, fs.ErrNotExist))
	is.Equal(len(files), 0)
}
