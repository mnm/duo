package gohtml_test

import (
	"testing"

	"github.com/matryer/is"
	"gitlab.com/mnm/duo/view/gohtml"
)

func TestPage(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page: gohtml.New("page.gohtml", `<h1>{{.}}</h1>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), "<h1>hi</h1>")
}

func TestLayout(t *testing.T) {
	t.SkipNow()
	is := is.New(t)
	view := &gohtml.View{
		Layout: gohtml.New("layout.gohtml", `<body>{{.}}</body>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), "<body></body>")
}

func TestPageLayout(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page:   gohtml.New("page.gohtml", `<h1>{{.}}</h1>`),
		Layout: gohtml.New("layout.gohtml", `<body>{{.}}</body>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), "<body><h1>hi</h1></body>")
}

func TestPageFrames(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page: gohtml.New("page.gohtml", `<h1>{{.}}</h1>`),
		Frames: []*gohtml.Template{
			gohtml.New("article.gohtml", `<article>{{.}}</article>`),
			gohtml.New("main.gohtml", `<main>{{.}}</main>`),
		},
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), "<main><article><h1>hi</h1></article></main>")
}
func TestPageFramesLayout(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page: gohtml.New("page.gohtml", `<h1>{{.}}</h1>`),
		Frames: []*gohtml.Template{
			gohtml.New("article.gohtml", `<article>{{.}}</article>`),
			gohtml.New("main.gohtml", `<main>{{.}}</main>`),
		},
		Layout: gohtml.New("layout.gohtml", `<body>{{.}}</body>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), "<body><main><article><h1>hi</h1></article></main></body>")
}

func TestErrPage(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page: gohtml.New("page.gohtml", `<h1>{{$.Hello.World}}</h1>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), `fallback error: template: page.gohtml:1:7: executing "page.gohtml" at <$.Hello.World>: can't evaluate field Hello in type string`)
}

func TestErrPageError(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page:  gohtml.New("page.gohtml", `<h1>{{$.Hello.World}}</h1>`),
		Error: gohtml.New("error.gohtml", `<h2>{{$.Error.Message}}</h2>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), `<h2>template: page.gohtml:1:7: executing &#34;page.gohtml&#34; at &lt;$.Hello.World&gt;: can&#39;t evaluate field Hello in type string</h2>`)
}

func TestErrPageLayoutError(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page:   gohtml.New("page.gohtml", `<h1>{{$.Hello.World}}</h1>`),
		Error:  gohtml.New("error.gohtml", `<h2>{{$.Error.Message}}</h2>`),
		Layout: gohtml.New("layout.gohtml", `<body>{{.}}</body>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), `<body><h2>template: page.gohtml:1:7: executing &#34;page.gohtml&#34; at &lt;$.Hello.World&gt;: can&#39;t evaluate field Hello in type string</h2></body>`)
}

func TestErrPageFramesLayout(t *testing.T) {
	is := is.New(t)
	view := &gohtml.View{
		Page: gohtml.New("page.gohtml", `<h1>{{$.Hello.World}}</h1>`),
		Frames: []*gohtml.Template{
			gohtml.New("article.gohtml", `<article>{{.}}</article>`),
			gohtml.New("main.gohtml", `<main>{{.}}</main>`),
		},
		Layout: gohtml.New("layout.gohtml", `<body>{{.}}</body>`),
	}
	html, err := view.Render("hi")
	is.NoErr(err)
	is.Equal(string(html), `<body><main><article>fallback error: template: page.gohtml:1:7: executing "page.gohtml" at <$.Hello.World>: can't evaluate field Hello in type string</article></main></body>`)
}

// Error in the frames
func TestPageErrFramesLayout(t *testing.T) {
	t.SkipNow()
}

// Error in the layout with frames
func TestPageFramesErrLayout(t *testing.T) {
	t.SkipNow()
}

// Error in the layout
func TestPageErrLayout(t *testing.T) {
	t.SkipNow()
}

// Error in the error
func TestPageErrError(t *testing.T) {
	t.SkipNow()
}
