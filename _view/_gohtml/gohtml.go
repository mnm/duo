package gohtml

import (
	"bytes"
	"html/template"
)

// New the code into a template
func New(path, code string) *Template {
	t := template.Must(template.New(path).Parse(code))
	return &Template{t}
}

type Template struct {
	*template.Template
}

func (t *Template) Render(props interface{}) ([]byte, error) {
	b := new(bytes.Buffer)
	if err := t.Execute(b, props); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

type View struct {
	Page   *Template
	Layout *Template
	Error  *Template
	Frames []*Template
}

func (v *View) Render(props interface{}) (html []byte, err error) {
	html, err = v.renderPage(props)
	if err != nil {
		return nil, err
	}
	for _, frame := range v.Frames {
		html, err = v.renderFrame(frame, template.HTML(html))
		if err != nil {
			return nil, err
		}
	}
	if v.Layout != nil {
		html, err = v.renderLayout(template.HTML(html))
		if err != nil {
			return nil, err
		}
	}
	return html, nil
}

func (v *View) renderPage(props interface{}) ([]byte, error) {
	html, err := v.Page.Render(props)
	if err != nil {
		return v.renderError(err)
	}
	return html, nil
}

func (v *View) renderFallbackError(err error) ([]byte, error) {
	return []byte("fallback error: " + err.Error()), nil
}

func (v *View) renderLayout(page template.HTML) (html []byte, err error) {
	html, err = v.Layout.Render(page)
	if err != nil {
		return v.renderError(err)
	}
	return html, nil
}

func (v *View) renderFrame(frame *Template, view template.HTML) (html []byte, err error) {
	html, err = frame.Render(view)
	if err != nil {
		return v.renderError(err)
	}
	return html, nil
}

func (v *View) renderError(err error) ([]byte, error) {
	if v.Error == nil {
		return v.renderFallbackError(err)
	}
	html, err := v.Error.Render(formatError(err))
	if err != nil {
		return v.renderFallbackError(err)
	}
	return html, nil
}

func formatError(err error) *errorProps {
	return &errorProps{
		Error: &Error{
			Message: err.Error(),
		},
	}
}

type errorProps struct {
	Error *Error
}

type Error struct {
	Message string `json:"message"`
}
