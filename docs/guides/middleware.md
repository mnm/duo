# Middleware

You can use middleware to hook into the request-response lifecycle. Every request flows down through each layer of middleware, then flows back up.

> TODO: Monodraw of Middleware

With middleware, you can

- Log requests
- Load and store user sessions
- Serve static files
- Throttle requests

... and much more.

At it's core, Duo's web server is just a stack of middleware. Unlike other frameworks, middleware is only added when you use features that need them. This improves performance and is aligned with [Only Pay for What You Use](./getting-started#Only-Pay-for-What-You-Use).

## Adding Middleware

You can add new middleware in the `middleware` package of your application

```
myapp
└── middleware
    └── web.go
```

To add middleware, create a public function that returns `func(next http.Handler) http.Handler`. The function's inputs support automatic [Dependency Injection](./dependency-injection).

Below I've created a simple response timer that depends on our logger:

```go
func Time(log log.Log) func(next http.Handler) http.Handler {
  return func(next http.Handler) http.Handler {
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
      now := time.Now()
      next.ServeHTTP(w, r)
      log.Info("response took %sms", time.Since(now))
    })
  }
}
```

The `Time` middleware is automatically appended to the middleware stack.

## Override the Middleware Stack

Occassionally you'll need more control over the order of the middleware stack. You can override Duo's default middleware stack by defining a `Middleware` function in the `web` package.

```
myapp
├── middleware
│   └── web.go
└── web
    └── web.go
```

The inputs to `Web` support automatic [Dependency Injection](./dependency-injection) and the result must implement the `middleware.Middleware` interface.

```go
package web

func Middleware(m middleware.Directory) middleware.Middleware {
  return middleware.Stack{
    m.Public,
    m.Session,
  }
}
```

### Middleware Directory

> TODO: Finish up explaining about the generated middleware directory
