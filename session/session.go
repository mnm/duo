package session

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/mnm/duo/internal/secretbox"
	"github.com/segmentio/ksuid"
	"github.com/tomasen/realip"
)

// Store interface
type Store interface {
	Write(w http.ResponseWriter, s *Session) (err error)
	Read(r *http.Request) (s *Session, err error)
}

// Encrypt a session
func Encrypt(secret string, session *Session) (value string, err error) {
	// Marshal the session into JSON.
	buf, err := json.Marshal(session)
	if err != nil {
		return "", err
	}
	// Encrypt the contents of the cookie.
	ciphertext, err := secretbox.Encode(secret, buf)
	if err != nil {
		return "", err
	}
	// Encode the ciphertext bytes into URL-safe Base64.
	value = base64.RawURLEncoding.EncodeToString(ciphertext)
	return value, nil
}

// Decrypt the session
func Decrypt(secret string, value string) (session *Session, err error) {
	// Decode the base64 encoding.
	ciphertext, err := base64.RawURLEncoding.DecodeString(value)
	if err != nil {
		return nil, err
	}
	// Decrypt the contents from the cookie.
	plaintext, err := secretbox.Decode(secret, ciphertext)
	if err != nil {
		return nil, err
	}
	// Unmarshal the plaintext back into a session.
	state := new(state)
	err = json.Unmarshal(plaintext, state)
	if err != nil {
		return nil, err
	}
	// TODO: Improve this interface, this is currently a partial session because
	// session needs data from the request.
	return &Session{
		state: state,
	}, nil
}

// Create a session from a request
func Create(r *http.Request) (*Session, error) {
	id, err := ksuid.NewRandom()
	if err != nil {
		return nil, err
	}
	return &Session{
		timeAway: time.Duration(-1),
		state: &state{
			ID:       id.String(),
			LastSeen: time.Now().Unix(),
			LastURL:  r.Header.Get("Referrer"),
			Data:     json.RawMessage(`{}`),
		},
	}, nil
}

// Refresh the session
func Refresh(s *Session, r *http.Request) {
	s.timeAway = time.Now().Sub(time.Unix(s.state.LastSeen, 0))
	s.userAgent = r.Header.Get("User-Agent")
	s.ipAddress = realip.FromRequest(r)
	s.currentURL = r.URL.String()
	s.newFlashes = nil
}

// Seal up the session
// TODO: come up with a better name for than Refresh and Seal.
func Seal(s *Session) {
	s.state.Flashes = s.newFlashes
	if s.state.Flashes == nil {
		s.state.Flashes = []*Flash{}
	}
	s.state.LastURL = s.currentURL
	s.state.LastSeen = time.Now().Unix()
}

type state struct {
	ID       string `json:"id,omitempty"`
	LastSeen int64  `json:"last_seen,omitempty"`
	LastURL  string `json:"last_url,omitempty"`

	Flashes []*Flash `json:"flashes,omitempty"`

	// custom session data
	Data json.RawMessage `json:"data,omitempty"`
}

// Session struct
type Session struct {
	ipAddress  string
	userAgent  string
	currentURL string
	newFlashes []*Flash
	timeAway   time.Duration
	state      *state
}

// ID returns the session ID.
func (s *Session) ID() string {
	return s.state.ID
}

// IP returns the user's IP address.
func (s *Session) IP() string {
	return s.ipAddress
}

// UserAgent returns the user's user agent.
func (s *Session) UserAgent() string {
	return s.userAgent
}

// LastSeen returns the time when this user was last seen in seconds
// since the unix epoch.
func (s *Session) LastSeen() int64 {
	return s.state.LastSeen
}

// TimeAway returns how long this user has been away (seconds ago).
// This will be -1 if it's a first-time user.
func (s *Session) TimeAway() time.Duration {
	return s.timeAway
}

// LastURL returns the previous URL this user visited on our site.
func (s *Session) LastURL() string {
	return s.state.LastURL
}

// Flashes returns all the flashes.
func (s *Session) Flashes() []*Flash {
	return s.state.Flashes
}

// Unmarshal the session data
func (s *Session) Unmarshal(v interface{}) error {
	return json.Unmarshal(s.state.Data, v)
}

// Marshal the session data
func (s *Session) Marshal(v interface{}) error {
	data, err := json.Marshal(v)
	if err != nil {
		return err
	}
	s.state.Data = data
	return err
}

var _ json.Marshaler = (*Session)(nil)
var _ json.Unmarshaler = (*Session)(nil)

// MarshalJSON fn
func (s *Session) MarshalJSON() ([]byte, error) {
	return json.Marshal(s.state)
}

// UnmarshalJSON fn
func (s *Session) UnmarshalJSON(b []byte) error {
	return json.Unmarshal(b, s.state)
}

// Flash a message
func (s *Session) Flash(kind string, message string) *Flash {
	flash := &Flash{
		Type:    kind,
		Message: message,
	}
	s.newFlashes = append(s.newFlashes, flash)
	return flash
}

// Flash struct
type Flash struct {
	Type    string `json:"type,omitempty"`
	Message string `json:"message,omitempty"`
}

// Flash implements error for convenience
func (f *Flash) Error() string {
	return f.Message
}
