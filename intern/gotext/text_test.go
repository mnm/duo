package text_test

import (
	"testing"

	"github.com/tj/assert"
	. "gitlab.com/mnm/duo/intern/gotext"
)

type fntest struct {
	fn  func(...string) string
	in  string
	out string
}

func TestSlim(t *testing.T) {
	tests := []fntest{
		{Slim, "", ""},
		{Slim, "hi world", "hiworld"},
		{Slim, "user id", "userid"},
		{Slim, "id user", "iduser"},
		{Slim, "id dns http", "iddnshttp"},
		{Slim, "http user", "httpuser"},
		{Slim, "string", "string_"},
		{Slim, "newIn", "newin"},
		{Slim, "new", "new_"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestPascal(t *testing.T) {
	tests := []fntest{
		{Pascal, "", ""},
		{Pascal, "hi world", "HiWorld"},
		{Pascal, "user id", "UserID"},
		{Pascal, "id user", "IDUser"},
		{Pascal, "id dns http", "IDDNSHTTP"},
		{Pascal, "http user", "HTTPUser"},
		{Pascal, "string", "String"},
		{Pascal, "newIn", "NewIn"},
		{Pascal, "new", "New"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestCamel(t *testing.T) {
	tests := []fntest{
		{Camel, "", ""},
		{Camel, "hi world", "hiWorld"},
		{Camel, "user id", "userID"},
		{Camel, "id user", "idUser"},
		{Camel, "id dns http", "idDNSHTTP"},
		{Camel, "http user", "httpUser"},
		{Camel, "string", "string_"},
		{Camel, "newIn", "newIn"},
		{Camel, "new", "new_"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestShort(t *testing.T) {
	tests := []fntest{
		{Short, "", ""},
		{Short, "hi world", "hw"},
		{Short, "user id", "ui"},
		{Short, "id user", "iu"},
		{Short, "id dns http", "idh"},
		{Short, "http user", "hu"},
		{Short, "string", "s"},
		{Short, "newIn", "ni"},
		{Short, "new", "n"},
		{Short, "newEwW", "new_"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestSnake(t *testing.T) {
	tests := []fntest{
		{Snake, "", ""},
		{Snake, "hi world", "hi_world"},
		{Snake, "user id", "user_id"},
		{Snake, "id user", "id_user"},
		{Snake, "id dns http", "id_dns_http"},
		{Snake, "http user", "http_user"},
		{Snake, "string", "string"},
		{Snake, "newIn", "new_in"},
		{Snake, "new", "new"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestUpper(t *testing.T) {
	tests := []fntest{
		{Upper, "", ""},
		{Upper, "hi world", "HI_WORLD"},
		{Upper, "user id", "USER_ID"},
		{Upper, "id user", "ID_USER"},
		{Upper, "id dns http", "ID_DNS_HTTP"},
		{Upper, "http user", "HTTP_USER"},
		{Upper, "string", "STRING"},
		{Upper, "newIn", "NEW_IN"},
		{Upper, "new", "NEW"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestPath(t *testing.T) {
	tests := []fntest{
		{Path, "", ""},
		{Path, "hi world", "hi/world"},
		{Path, "user id", "user/id"},
		{Path, "id user", "id/user"},
		{Path, "id dns http", "id/dns/http"},
		{Path, "http user", "http/user"},
		{Path, "string", "string"},
		{Path, "newIn", "new/in"},
		{Path, "new", "new"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestPlural(t *testing.T) {
	tests := []fntest{
		{Plural, "", ""},
		{Plural, "hi world", "hi worlds"},
		{Plural, "user id", "user ids"},
		{Plural, "id user", "id users"},
		{Plural, "id dns http", "id dns https"},
		{Plural, "http user", "http users"},
		{Plural, "string", "strings"},
		{Plural, "newIn", "newIns"},
		{Plural, "news", "news"},
		{Plural, "new", "news"},
		{Plural, "hi worlds", "hi worlds"},
		{Plural, "Hi World", "Hi Worlds"},
		{Plural, "Hi$World$$$", "Hi$Worlds$$$"},
		{Plural, "hi_world", "hi_worlds"},
		{Plural, "his_world", "his_worlds"},
		{Plural, "his", "his"},
		{Plural, "hi", "his"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}

func TestSingular(t *testing.T) {
	tests := []fntest{
		{Singular, "", ""},
		{Singular, "hi worlds", "hi world"},
		{Singular, "user ids", "user id"},
		{Singular, "users ids", "users id"},
		{Singular, "id users", "id user"},
		{Singular, "dns", "dn"},
		{Singular, "id dns https", "id dns http"},
		{Singular, "id dns http", "id dns http"},
		{Singular, "http users", "http user"},
		{Singular, "strings", "string"},
		{Singular, "newIns", "newIn"},
		{Singular, "news", "news"},
		{Singular, "new", "new"},
		{Singular, "hi world", "hi world"},
		{Singular, "Hi Worlds", "Hi World"},
		{Singular, "Hi$Worlds$$$", "Hi$World$$$"},
		{Singular, "hi_worlds", "hi_world"},
		{Singular, "his_worlds", "his_world"},
		{Singular, "his", "hi"},
	}
	for _, test := range tests {
		assert.Equal(t, test.out, test.fn(test.in), "%s != %s", test.in, test.out)
	}
}
