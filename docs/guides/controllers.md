---
---

# Controllers

## Introduction

Controllers are the coordinators of our web application. Controllers receive requests from the router, fetch data from a database or model, then respond with JSON or render a view.

## Folder Structure

Controllers go in the `controller/` directory. Each controller has it's own package. Controllers can be nested.

```
myapp
└── controller
    ├── controller.go          -> root controller
    ├── posts
    │   ├── posts.go           -> posts controller
    │   └── comments
    │       └── comments.go    -> comments controller
    └── users
        └── users.go           -> users controller
```

## Actions

Each controller has zero or more actions. Actions handle incoming requests.

Let's start with a users controller

```go
package users

// User data
type User struct {
  ID   int
  Name string
  Age  int
}

// User index
func Index() ([]*User, error) {}

// Create a user
func Create(name string, age int) (*User, error) {}

// Show a user
func Show(id int) (*User, error) {}

// Update a user
func Update(id int, name string, age int) error {}

// Delete a user
func Delete(id int) error {}
```

An action's signature defines what kind of request an action will accept and what kind of response to expect back.

Zooming into the `Create` action:

```go
func Create(name string, age int) (*User, error) {}
```

This `Create` action accepts a name that's a string and an age that's an integer and returns either a User or an error.

We'll cover how requests are validated and responses are built in more detail in later sections, but first let's learn how request data gets passed into an action.

### Inputs

Your actions wouldn't be very useful without access to the request data. An input map is built from each request and validated by the action's input schema before an action is called.

#### Accessing Request Data

Your actions wouldn't be very useful without access to the request data. Each request is unmarshaled into a key-value pair that is validated by the action's signature before the action is called.

The request map uses data from 3 parts of the request.

|   Request | Description                                 | Example            |
| --------: | :------------------------------------------ | :----------------- |
|  **Path** | Dynamic segments in the router path         | `/users/:id`       |
| **Query** | The URL's key-value pairs after the `?`     | `?id=10&order=asc` |
|  **Body** | The request body. Either form or JSON data. | `{ "id": 10 }`     |

> Note: The request body is ignored for `GET` requests.

##### Query Parameters

To get a better feel for this, let's look at this `GET` request to `/posts/:id/comments`:

```http
GET /posts/10/comments?order=asc&author=Alice
```

The `:id` path value is combined with the order and author query parameters, resulting in the following request map:

```json
{
  "id": 10,
  "order": "asc",
  "author": "Alice"
}
```

##### JSON Body

A `POST` request to `/posts/:id/comments` with a request body follows a similar pattern.

```http
POST /posts/10/comments?author=Alice HTTP/1.1
Content-Type: application/json

{
  "email": "alice@duo.build"
}
```

Results in a request map that looks like this:

```json
{
  "id": 10,
  "author": "Alice",
  "email": "alice@duo.build"
}
```

> Note: If no `Content-Type` provided, the json body will be ignored.

##### Form Body

The request body can also be form data. For the same request:

```http
POST /posts/10/comments?author=Alice HTTP/1.1
Content-Type: application/x-www-form-urlencoded

email=alice%40duo.build
```

The resulting request map looks like this:

```json
{
  "id": 10,
  "author": "Alice",
  "email": "alice@duo.build"
}
```

> Note: If no `Content-Type` is provided, the form body will be ignored.

##### Key conflicts

Occassionally the same key has different values. The request map priority is:

1. Path values _(highest priority)_
2. Query parameters
3. Request body _(lowest priority)_

For a POST request to `/posts/:id/comments`

```http
POST /posts/10/comments?id=20&author=Alice
```

With the request body

```json
{
  "id": 30,
  "author": "Bob"
}
```

The request map resolves to:

```json
{
  "id": 10,
  "author": "Bob"
}
```

## Action Inputs

Once we have the request map, we validate the map with the **input definition**. If the request map doesn't satisfy the input definition, the request is rejected with a `400 Bad Request` response error and the request will never reach the action.

Let's understand the different ways to define the input definition.

### List of Parameters

A list of parameters are folded into a single input definition:

```go
func Create(name string, email string) {}
```

Both `name` and `email` are **required**. An acceptable request map looks like this

```json
{
  "name": "Alice",
  "email": "alice@duo.build"
}
```

Any extra fields that aren't present in the request map will be ignored:

```json
{
  "name": "Alice",
  "email": "alice@duo.build",
  "extra": "fields" // are ignored
}
```

And if we send an invalid request map:

```json
{
  "name": 123
}
```

We'll get back a `400 Bad Request` with a helpful error:

```json
{
  "error": "name must be a string and email can't be blank",
  "fields": {
    "name": "must be a string",
    "email": "can't be blank"
  }
}
```

### Single Input Structs

Defining the input schema with parameters gets unwieldy for large actions, so you can also define your input definition with a single struct.

Unlike a list of parameters, single input structs are not nested under the parameter name.

```go
type Input struct {
  ID    int
  Name  string
  Email string
}

func Update(in *Input) {}
```

An acceptable input map for this input schema could be:

```json
{
  "ID": 10,
  "Name": "Alice",
  "Email": "alice@duo.build"
}
```

Notice how the input schema is **not nested** under the `in` parameter.

### `json` Struct Tag

You can adjust an input definition's keys with the `json` struct tag:

```go
type Input struct {
  ID    int    `json:"id"`
  Name  string `json:"name"`
  Email string `json:"email_address"`
}

func Update(in *Input) {}
```

The struct above accepts an input map like:

```json
{
  "id": 10,
  "name": "Alice",
  "email_address": "alice@duo.build"
}
```

### Complex Input Parameters

You can also define actions with more complex parameters. For the signature below:

```go
type Email string

type Operation struct {
  Name   string `json:"name"`
  Params []*Param `json:"params"`
}

type Param struct {
  Version int  `json:"version"`
  Update  bool `json:"update"`
}

func Update(id string, email Email, op *Operation) {}
```

The struct fields are **recursively required**, so an acceptable request map could be:

```json
{
  "id": 10,
  "email": "alice@duo.build",
  "op": {
    "name": "update",
    "params": [
      {
        "version": 1,
        "update": true
      },
      {
        "version": 2,
        "update": false
      }
    ]
  }
}
```

And if you forgot the second `version` property, you'll be greeted with a `400 Bad Request` and this helpful error message:

```json
{
  "error": "version can't be blank",
  "fields": {
    "op.params[1].version": "can't be blank"
  }
}
```

### Optionals

Optional inputs are prefixed with a pointer:

```go
func Create(name string, *email string) {}
```

Both of these request maps are acceptable:

```json
{
  "name": "Bob",
}

// or

{
  "name": "Bob",
  "email": "bob@duo.build"
}
```

To make a struct optional, you use the mythical double-pointer:

```go
type Settings struct {
  VIP bool `json:"vip"`
}

func Create(name string, settings **Settings) {}
```

Either of these input maps are ok

```json
{
  "name": "Bob",
}

// or

{
  "name": "Bob",
  "settings": {
    "vip": true
  }
}
```

If an optional struct is present, its keys are required. To make the keys optional too, add another pointer:

```go
type Settings struct {
  VIP *bool `json:"vip"`
}

func Create(name string, settings **Settings) {}
```

Now, all of these request maps are acceptable:

```json
{
  "name": "Bob",
}

// or

{
  "name": "Bob",
  "settings": {
    "vip": true
  }
}

// or

{
  "name": "Bob",
  "settings": {}
}
```

### Custom Validation

For more complex validation, you can define a `Validate` method on an input type:

```go
type Input struct {
  Name  string `json:"name"`
  Email string `json:"email"`
}

func (i *Input) Validate() error {
  if len(i.Name) < 3 {
    return fmt.Errorf("%s is not long enough", i.Name)
  } else if !strings.Contains(i.Email, "@") {
    return fmt.Errorf("%s is not a valid email", i.Email)
  }
  return nil
}

func Create(in *Input) {}
```

If validation fails, you get a `400 Bad Request` response with your error message:

```json
{
  "error": "name is not long enough"
}
```

Validation also works with other input types:

```go
type Email string

func (email Email) Validate() error {
  if !strings.Contains(string(email), "@") {
    return errors.New("is not valid")
  }
  return nil
}

func Create(email Email) {}
```

If validation fails, you get a `400 Bad Request` response. Since `email` is a key within the input map, we can provide more information in the error message:

```json
{
  "error": "email is not valid",
  "fields": {
    "email": "is not valid"
  }
}
```

### Passing in `context.Context`

`context.Context` is useful for gracefully canceling requests. For each action, you can optionally pass in a `context.Context` type as the first parameter. This context comes from the request.

```go
func Create(ctx context.Context, in *Input) {}
func Delete(ctx context.Context, id int) {}
func Notify(ctx context.Context) {}
```

## Action Outputs

An action's results define the **Output Definition**. The output definition determines the shape of the data returned to the requester.

### Named Results

Named results get folded into a single return object. The names are the result keys.

```go
func Create() (id int, email string) {}
```

The JSON response for `Create` would look like this: \

```json
{
  "id": 10,
  "email": "alice@duo.build"
}
```

### Single Output Struct

You can also define your output with a single struct.

```go
type Output struct {
	  ID int
  Email string
}

func Create() *Output {}
```

The JSON response for `Create` would look like this: \

```json
{
  "ID": 10,
  "Email": "alice@duo.build"
}
```

Of course, you can adjust an output keys with the `json` struct tag:

```go
type Output struct {
	  ID    int    json:"id"
  Email string json:"email_address"
}

func Create() (out *Output) {}
```

Resulting in a JSON response like this:

```json
{
  "id": 10,
  "email_address": "alice@duo.build"
}
```

Notice how the JSON response is **not** wrapped in "`out"`? Single structs are special because they ignore the result name.

### Unnamed Results

Unlike inputs, outputs can have unnamed results.

```go
func Create() (int, string) {}
```

The JSON result for this signature looks like:

```json
[10, "alice@duo.build"]
```

### Single Unnamed Result

Single value unnamed results behave a little differently:

```go
func Create() string {}
```

The JSON response for this signature looks like:

```json
"alice@duo.build"
```

Single unnamed results **don't** wrap the value in an array.

### Error Result

All of the output signatures above support an optional error type as their last parameter. Below are all valid examples:

- **Named Results** `func Create() (id int, email string, err error) {}`
- **Single Output Struct** `func Create() (*Output, error) {}`
- **Unnamed Results** `func Create() (int, string, error) {}`
- **Single Unnamed Result** `func Create() (string, error) {}`

For actions that return an error:

```go
func Create() (*Output, error) {
  return nil, errors.New("unable to create user")
}
```

The JSON response for this action would be:

```json
{
  "error": "unable to create user"
}
```

## Action Receiver

Our actions wouldn't be very useful without access to shared dependencies like loggers, database clients and user sessions. To support this, every action can optionally define an action receiver. The receiver is the place to put an action's dependencies.

Below we've defined the `Controller` receiver:

```go
package users

type Controller struct {
  Log log.Log
  DB *sql.DB
}

func (c *Controller) Create(name string, age int) (*User, error) {}

func (c *Controller) Show(id int) (*User, error) {}
```

You can also use functions to initialize receivers. We typically use the `New` function to initialize receivers, but the function name can be anything:

```go
package users

func New(log log.Log, db *sql.DB) *Controller {
  return &Controller{log, db}
}

type Controller struct {
  log log.Log
  db  *postgres.Client
}

func (c *Controller) Create(name string, age int) (*User, error) {}

func (c *Controller) Show(id int) (*User, error) {}
```

The same receiver doesn't need to be shared across every action, you can mix action receivers within a controller or have some actions with no receiver at all:

```go
type Creator struct {
  Queue *sqs.Queue
}

// Creator receiver
func (c *Creator) Create(name string, age int) (*User, error) {}

type Editor struct {
  Email *mailgun.Client
}

// Editor receiver
func (e *Editor) Edit(id int) (*User, error) {}

// No receiver
func New() {}
```

These dependencies are initialized and wired up using [Dependency Injection](./dependency-injection).

### Using `*http.Request` and `http.ResponseWriter`

Action receivers can also access to the request and response.

```go
type Controller struct {
  *http.Request
  http.ResponseWriter
  Log log.Log
  DB *sql.DB
}

func (c *Controller) Create() {
  c.Write([]byte(c.URL.Path)
}
```

Duo is smart enough to initialize dependencies that don't depend on the request or response only once. In the example above, `log.Log` and `*sql.DB` are initialized only once and shared across every request.

If your action receiver has a dependency that depends on `*http.Request` or `http.ResponseWriter`, it will be initialize for each request.

```go
type Session struct {
  *http.Request
  http.ResponseWriter
}

type Controller struct {
  Session *Session
  Log log.Log
  DB *sql.DB
}

func (c *Controller) Create() {
  c.Write([]byte(c.URL.Path)
}
```

In the example above `*Session` will be initialized every request, `log.Log` and `*sql.DB` will be initialized only once.

## Content Negotiation

> TODO: Content Negotation
>
> - What's the default for a browser?
> - JSON / HTML / Custom Marshaler
> - GraphQL support with GoHTML-style Output functions

## Using an HTTP Handler

Sometimes you need to work with Go's standard `http.Handler`. Any action can also use this standard signature.

```go
func Create(r *http.Request, w http.ResponseWriter) {}
```

This also works with an [Action Receiver](#Action-Receiver).

```go
func (c *Controller) Create(r *http.Request, w http.ResponseWriter) {}
```

Request mapping is disabled for these signatures. You're entirely in control of dealing with the request and how to respond.
