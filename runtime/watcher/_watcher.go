package watcher

import (
	"io/fs"
	"sync"

	"github.com/fsnotify/fsnotify"
)

func New(fsys fs.FS) *Watcher {
	return &Watcher{
		fsys:     fsys,
		watchset: map[string]struct{}{},
		watcher:  nil,
	}
}

type Watcher struct {
	fsys     fs.FS
	mu       sync.RWMutex
	watchset map[string]struct{}
	watcher  *fsnotify.Watcher // can be nil
}

type Notification struct {
	Path  string
	Event Event
}

// Enable the watcher
func (w *Watcher) Enable() (err error) {
	w.mu.Lock()
	defer w.mu.Unlock()
	// Nothing to do
	if w.watcher != nil {
		return nil
	}
	w.watcher, err = fsnotify.NewWatcher()
	if err != nil {
		return err
	}
	for path := range w.watchset {
		if err := w.watcher.Add(path); err != nil {
			return err
		}
	}
	return nil
}

// Disable the watcher
func (w *Watcher) Disable() error {
	w.mu.Lock()
	defer w.mu.Unlock()
	if w.watcher == nil {
		return nil
	}
	return w.watcher.Close()
}

// Add a path
func (w *Watcher) Add(pattern string) error {
	w.mu.Lock()
	defer w.mu.Unlock()
	// glob
	w.watchset[pattern] = struct{}{}
	if w.watcher == nil {
		return nil
	}
	return w.watcher.Add(pattern)
}

// Remove a path
func (w *Watcher) Remove(pattern string) error {
	w.mu.Lock()
	defer w.mu.Unlock()
	delete(w.watchset, pattern)
	if w.watcher == nil {
		return nil
	}
	return w.watcher.Remove(pattern)
}

// Wait for a notification
func (w *Watcher) Wait() <-chan *Notification {
	panic("Wait: not implemented")
}
