package secretbox_test

import (
	"encoding/base64"
	"testing"

	"gitlab.com/mnm/duo/internal/secretbox"
	"github.com/tj/assert"
)

const secret = "My super secret"
const secret2 = "Another secret yo"

func init() {
	secretbox.Nonce = func() (b [24]byte, err error) {
		nonce, err := base64.StdEncoding.DecodeString(`HumO/P0YxLSv9hlc5Syb/QPBU+NXjC2K`)
		if err != nil {
			return b, err
		}
		copy(b[:], nonce)
		return b, nil
	}
}

func TestOk(t *testing.T) {

	ciphertext, err := secretbox.Encode(secret, []byte("10"))
	assert.NoError(t, err)
	assert.Equal(t, `HumO/P0YxLSv9hlc5Syb/QPBU+NXjC2Kn3G1zQdpAddrbE/DXsQih6fT`, base64.StdEncoding.EncodeToString(ciphertext))
	plaintext, err := secretbox.Decode(secret, ciphertext)
	assert.NoError(t, err)
	assert.Equal(t, "10", string(plaintext))
}

func TestNotOk(t *testing.T) {
	ciphertext, err := secretbox.Encode(secret, []byte("10"))
	assert.NoError(t, err)
	assert.Equal(t, `HumO/P0YxLSv9hlc5Syb/QPBU+NXjC2Kn3G1zQdpAddrbE/DXsQih6fT`, base64.StdEncoding.EncodeToString(ciphertext))
	_, err = secretbox.Decode(secret2, ciphertext)
	assert.EqualError(t, err, "failed to decrypt given message")
}
