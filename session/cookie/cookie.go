package cookie

import (
	"net/http"

	"gitlab.com/mnm/duo/session"
)

// Three months is the max age without configuration
const threeMonths = 90 * 24 * 60 * 60

// New initializes a session store
func New(name, secret string) *Store {
	return &Store{name, secret, &Config{
		Path:     "/",
		HTTPOnly: true,
		Secure:   false,
		SameSite: http.SameSiteLaxMode,
		MaxAge:   threeMonths,
	}}
}

// NewConfig initializes a session store with additional configuration.
func NewConfig(name, secret string, config *Config) *Store {
	return &Store{name, secret, config.defaults()}
}

// Config struct
type Config struct {
	Path     string
	HTTPOnly bool
	Secure   bool
	SameSite http.SameSite
	Domain   string
	MaxAge   int
}

// Config defaults
func (c *Config) defaults() *Config {
	if c.Path == "" {
		c.Path = "/"
	}
	if c.SameSite == 0 {
		c.SameSite = http.SameSiteLaxMode
	}
	return c
}

// Store struct
type Store struct {
	name   string
	secret string
	config *Config
}

var _ session.Store = (*Store)(nil)

// Read from the session store
func (store *Store) Read(r *http.Request) (s *session.Session, err error) {
	cookie, err := r.Cookie(store.name)
	if err != nil {
		if err == http.ErrNoCookie {
			return session.Create(r)
		}
	}
	s, err = session.Decrypt(store.secret, cookie.Value)
	if err != nil {
		// Ignore decryption errors, this can happen when
		// the session envelope changes.
		return session.Create(r)
	}
	// Existing session, refresh and return
	session.Refresh(s, r)
	return s, nil
}

// Write to the session store
func (store *Store) Write(w http.ResponseWriter, s *session.Session) (err error) {
	session.Seal(s)
	value, err := session.Encrypt(store.secret, s)
	if err != nil {
		return err
	}
	config := store.config
	http.SetCookie(w, &http.Cookie{
		Name:     store.name,
		Value:    value,
		Path:     config.Path,
		HttpOnly: config.HTTPOnly,
		Secure:   config.Secure,
		SameSite: config.SameSite,
		Domain:   config.Domain,
		MaxAge:   config.MaxAge,
	})
	return nil
}
