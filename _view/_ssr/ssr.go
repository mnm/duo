package ssr

import (
	"encoding/json"

	"gitlab.com/mnm/duo/js"
	v8 "gitlab.com/mnm/duo/js/v8"

	"gitlab.com/mnm/duo/view"
)

// New SSR view list. This low-level library depends on the builder to build a
// single script exposing a `render(path, props)` signature. We bundle the SSR
// views together so we only need to load 1 script across the pool of V8
// isolates. Path can be anything you want, code is the bundled script.
func New(path, code string) *Views {
	pool := v8.New()
	if err := pool.Script(path, code); err != nil {
		// TODO: remove this panic. Now that pool is now lazy, I can probably remove
		// *Views entirely and just use *View
		panic(err)
	}
	return &Views{pool}
}

type Views struct {
	*v8.Pool
}

var _ js.VM = (*Views)(nil)

// View struct
type View struct {
	Engine js.VM
	Page   string

	// Data is a pre-rendered string to make it more direct to pass into render
	//
	// Data has the following shape:
	// type Data = {
	//   path:   string
	//   layout: { path: string }
	//   error:  { path: string }
	//   frames: { path: string }[]
	// }
	//
	Data string
}

var _ view.View = (*View)(nil)

// Render the view
func (v *View) Render(props interface{}) ([]byte, error) {
	data, err := json.Marshal(props)
	if err != nil {
		return nil, err
	}
	result, err := v.Engine.Eval(v.Page, `exports.render(`+v.Data+`, `+string(data)+`)`)
	if err != nil {
		return nil, err
	}
	return []byte(result), nil
}
