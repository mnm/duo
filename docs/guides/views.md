---
state: draft
---

# Views

- `layout.svelte`, `error.svelte` & `frame.svelte` are special filenames.
- layouts override each other
- error pages inherit from the layout, not other error pages
  - treat them as fallback views
- frames can be nested like a tree using slots
- between layout and top frame is where we should hydrate
  - if there is no frame, between layout and page
- file linking only works within the same file extension
  - layout.gohtml would be ignored by index.svelte
- layouts support a head slot that you can append to
- capital names are not exposed by default, they are for components
- views without actions are rendered by default
  - doesn't need to be RESTful
- underscored views are ignored
- gohtml, html, svelte, jsx, css, js
  - css, js are not entries but can be relative to the HTML
  - should this work with images too? And what's the difference with `public/`?
- route paths and flash data are available as variables and helpers
  - passed in as props for svelte, react and gohtml
- minify, optimize when running duo build
- support importing CSS variables somehow
- use svelte:head to extend the head within sublayouts
- use defer and async for scripts inside svelte:head

## Introduction

Views are what our users see. Views are found in the `view/` directory.

Duo uses a pluggable template engine to support many different types of views.

You can change the template engine by changing the file extension. Support for `.html`, `.gohtml` come with the framework.

## Render Engine

> TODO: Engine Interface

```go
package view

func New(engine *render.Engine) *View {

}

type View struct {

}

func (v *View) Render(path string, props interface{}) ([]byte, error) {

}
```

## Routing

Views have default routing for resource views. The default route depends on the name of the view.

| HTTP Route            | `view/`              | Description    |
| :-------------------- | :------------------- | :------------- |
| `GET /users`          | `users/index.gohtml` | List all users |
| `GET /users/new`      | `users/new.gohtml`   | New user form  |
| `GET /users/:id`      | `users/show.gohtml`  | Show a user    |
| `GET /users/:id/edit` | `users/edit.gohtml`  | Edit user form |

By changing the file extension, you change how the view is rendered. You can always override default routing in the [Router](./03-router.md).

To avoid conflicting views, you can't have the same view with different file extensions.

```
view
├── index.gohtml
└── index.html   -> Compiler error
```

All other views within `view/` are either [Components](#Components), [Layouts](#Layouts) or [Errors](#Errors).

For a single page, you can define an `index` view within a subdirectory.

| HTTP Route              | `view/`                         |
| :---------------------- | :------------------------------ |
| `GET /about`            | `about/index.gohtml`            |
| `GET /settings/privacy` | `settings/privacy/index.gohtml` |
| `GET /posts/:id`        | `posts/show.gohtml`             |

Views do not need an accompanying controller. The controller is used to pass data into the view, but if you don't need to pass data into the view, you can create a view without a controller.

## Components

Components are self-contained view fragments.

> TODO: Components

## Layouts

> TODO: Layouts

## Errors

> TODO: Errors

## Static Pages

> TODO: Static Pages
