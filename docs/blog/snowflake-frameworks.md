# Snowflake Frameworks

- You already have a framework

# Why a Framework?

This post is for the anti-framework readers among us. I was in your camp for a long time, so I

## Why Duo

Duo was born one late night in March 2020. I was watching Laracasts by Jeffery Way. He was building an e-commerce website using the Laravel web framework. I was blown away by how efficient he was at putting together a complex website.

As a member of the anti-framework, modular Node.js and Go ecosystems, I compared how I would build the same website with those languages. I made a list:

- Next.js for the frontend server
- Go for the API server

For Laravel developers, these is a couple keystrokes away.

For a long time, I prided myself on keeping everything modular, single-responsibility code libraries. Everything should be swappable. You should only bring in what you need. Each library should be the best at what it does. This is good software development after all.

And it is! The missing piece is that you need to connect all these libraries together. Sometimes these libraries snap together snuggly, other times you're gluing them together. This glue code lives in your application and changes for each module. The framework could be this glue.

I'm always looking for ways of building better websites with less resources. Maybe it's the rugged individualistic American in me, but I love the idea of building something big and beautiful from nothing on my own.

After building a number of websites, I realized that I was carrying many of these modular libraries forward for each project. I had environment module, RPC component, database access library. I also used a lot of the same community modules, because I knew how to use them.

I had basically built an mini framework of software packages I understood and was comfortable with. This combination of packages is unique and something only I understood.

Since I had already built this snowflake framework, I wondered what other assumptions I could make if I assumed these packages existed and knew in advance where they lived and what their API was.

It turns out quite a bit: You can scaffold code, you can write glue code that depends on the existence of an environment or a database.

Once I realized I could combine all these packages and

Another issue with the snowflake framework is onboarding other developers. to the codebase requires a long sit-down to explain how the modules fit together with debates about why we chose this package and not that package.

<!-- This glue lives in your application. e -->

Each library should be isolated from each other. After all, this is good software development.

## What's Wrong with Frameworks?

## What does Duo do to address these issues?
