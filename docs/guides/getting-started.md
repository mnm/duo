---
next: [Controllers](./controllers.md)
---

# Getting Started

## What is Duo?

Duo is a full-stack web framework written for Go that makes building audacious web applications much easier.

Duo's design optimizes for 3 parts of your applications life-cycle.

- Launch: Radically Less Code
- Maintain: Type-safety and compiler guarantees
- Deploy: Simpler Devops

Let's learn why.

### Duo is a Framework Compiler

Programming languages like Go are compiled from high-level languages with loops and conditionals to low-level Assembly instructions like adds and jumps.

Duo moves up a layer. Duo is a compiler for your entire web application. It takes your project as input and outputs a web application binary. You can write the high-level application code and let Duo compile, optimize and generate low-level server code.

Thinking about a framework as a compiler is a fundamental shift from the frameworks that have come before.

Duo's compiler gives you the productivity gains of working at a high-level without any of the tradeoffs in runtime performance.

You'll write radically less code with Duo. You'll be able to take on more ambitious projects and spend fewer resources. I can't wait to see what you build.

> TODO: This needs to be more tailored to the customer: engineering managers and tech leads that decide on a team's technology choices.
>
> - Enterprise wants the whole package. Buying solutions and full workflow.
> - Reduce the mental overhead. Don't need to make all the decision. Don't need > to spend hours on Github. All the pieces of
> - Dependency and version management. Teammates bringing up to speed.
>   - 40-50 dependencies in GraphCDN.
> - Why is Supabase so successful?
> - Open-source Firebase Alternative
> - E.g. Laravel for Go.
>   - Getting PHP => Go

## One Program, Many Entrances

## The Wires are out of your Way

<!-- About how we hide configuration, dependency injection -->
<!-- But not in the wall. you still can access them if needed -->

### Only Pay for What You Use

> TODO Only Pay for What You Use. Optional Complexity

## Gradual Ramp

## Cloud Agnostic

## Frontend Agnostic

## Why Duo?

Duo is built for the future with respect to the past. Duo blends some great ideas from other frameworks:

- Rails teaches us how to fully embrace the HTTP protocol and web standards.
- Laravel widens our ambition. A complete web framework needs queues, mailers, schedulers and tasks and more.
- Next.js doesn't overwhelm you with a mountain of files upfront. The framework grows as your needs grow.

Duo builds on these wonderful ideas with some ideas of it's own.

## Design Principles

### Modular Monolith

> TODO: Modular Monolith

### Type-Safety Everywhere

> TODO Type-Safety Everywhere is about using code generation to achieve better type safety

### Loose Coupling

> TODO Loose Coupling through dependency injection.

### Feels Natural

> TODO The way you write Duo should feel like writing idiomatic Go

## Deploy Anywhere

> Serverless, Serverful

## Installing Duo

Duo ships as a single binary that runs on Linux and Mac. Windows support is coming soon.

The easiest way to get started is by copying and pasting the command below in your terminal:

```sh
curl gitlab.com/mnm/duo/install | sh
```

This script will download the right binary for your operating system and move the binary to the right location in your `$PATH`.

Confirm that you've installed Duo by typing `duo` in your terminal.

```sh
$ duo help
```

You should see the following:

```sh
Usage:

duo [<flags>] <command> [<args> ...]

Flags:

  -h, --help Output usage information.
  -C, --chdir=CHDIR Change directory

Commands:

  help Show help for a command.
  build build application.
  run run the application.
```

## Your First Project

Now that we have duo installed, let's create a basic headless blog.

```sh
$ duo new blog
$ cd blog/
```

You'll notice that the directory is empty. What gives? One of Duo's tenets is [Optional Complexity](#Optional-Complexity). Since we haven't written any code yet, there's no framework. The framework is built _around_ our application code.

Let's start our application server:

```sh
$ duo run
Listening on http://localhost:3000
```

And if we go to http://localhost:3000

```sh
Welcome to Duo!
```

### Hello world

In a separate terminal, let's create our first controller

```sh
$ mkdir controller
$ touch controller/controller.go
```

Inside that controller, create an `Index` action that returns a string

```go
package controller

func Index() string {
  return "hello world!"
}
```

And now you'll see

```
hello world!
```

### The `duo` directory

You may have noticed that a `duo` directory was created automatically after you created your controller. The `duo` directory contains the framework's generated code. We'll occasionally import generated packages from this directory, but for the most part you can ignore the `duo` directory.

This directory should usually be excluded from source control. If you're using `git`, a `.gitignore` file will be created and populated automatically with the `duo` directory.

### Creating a Post

> TODO: finish first project
