package glob

import (
	"sort"

	"github.com/gobwas/glob"
)

type Set struct {
	patterns map[string]glob.Glob
}

func (s *Set) Add(patterns ...string) (err error) {
	if s.patterns == nil {
		s.patterns = map[string]glob.Glob{}
	}
	for _, pattern := range patterns {
		if s.patterns[pattern] != nil {
			continue
		}
		s.patterns[pattern], err = glob.Compile(pattern)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Set) List() (globs []glob.Glob) {
	sorted := []string{}
	for pattern := range s.patterns {
		sorted = append(sorted, pattern)
	}
	sort.Strings(sorted)
	for _, pattern := range sorted {
		globs = append(globs, s.patterns[pattern])
	}
	return globs
}
