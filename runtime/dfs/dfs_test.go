package dfs_test

import (
	"errors"
	"io/fs"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"

	"github.com/matryer/is"
	"gitlab.com/mnm/duo/runtime/dfs"
)

func View() map[string]dfs.Generator {
	return map[string]dfs.Generator{
		"duo/view": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			dir.Entry("index.svelte", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
				file.Write([]byte(`<h1>index</h1>`))
				return nil
			}))
			dir.Entry("about/about.svelte", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
				file.Write([]byte(`<h2>about</h2>`))
				return nil
			}))
			return nil
		}),
	}
}

func TestFS(t *testing.T) {
	is := is.New(t)

	// 1. duo
	df := dfs.New(nil, View())
	file, err := df.Open("duo")
	is.NoErr(err)
	rdf, ok := file.(fs.ReadDirFile)
	is.True(ok)
	des, err := rdf.ReadDir(-1)
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "view")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)
	fi, err := des[0].Info()
	is.NoErr(err)
	is.Equal(fi.IsDir(), true)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.ModeDir)
	is.Equal(fi.Name(), "view")
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	stat, err := file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "duo")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// Stat duo
	df = dfs.New(nil, View())
	stat, err = fs.Stat(df, "duo")
	is.NoErr(err)
	is.Equal(stat.Name(), "duo")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// ReadDir duo
	df = dfs.New(nil, View())
	des, err = fs.ReadDir(df, "duo")
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "view")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)

	// 2. duo/view
	df = dfs.New(nil, View())
	file, err = df.Open("duo/view")
	is.NoErr(err)
	rdf, ok = file.(fs.ReadDirFile)
	is.True(ok)
	des, err = rdf.ReadDir(-1)
	is.NoErr(err)
	is.Equal(len(des), 2)
	is.Equal(des[0].Name(), "about")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about")
	is.Equal(fi.IsDir(), true)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.ModeDir)
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	is.Equal(des[1].Name(), "index.svelte")
	is.Equal(des[1].IsDir(), false)
	is.Equal(des[1].Type(), fs.FileMode(0))
	fi, err = des[1].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "index.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "view")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// Stat duo
	df = dfs.New(nil, View())
	stat, err = fs.Stat(df, "duo/view")
	is.NoErr(err)
	is.Equal(stat.Name(), "view")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// ReadDir duo
	df = dfs.New(nil, View())
	des, err = fs.ReadDir(df, "duo/view")
	is.NoErr(err)
	is.Equal(len(des), 2)
	is.Equal(des[0].Name(), "about")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about")
	is.Equal(fi.IsDir(), true)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.ModeDir)
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	is.Equal(des[1].Name(), "index.svelte")
	is.Equal(des[1].IsDir(), false)
	is.Equal(des[1].Type(), fs.FileMode(0))
	fi, err = des[1].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "index.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)

	// 3. duo/view/about
	df = dfs.New(nil, View())
	file, err = df.Open("duo/view/about")
	is.NoErr(err)
	rdf, ok = file.(fs.ReadDirFile)
	is.True(ok)
	des, err = rdf.ReadDir(-1)
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "about.svelte")
	is.Equal(des[0].IsDir(), false)
	is.Equal(des[0].Type(), fs.FileMode(0))
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "about")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// Stat duo
	df = dfs.New(nil, View())
	stat, err = fs.Stat(df, "duo/view/about")
	is.NoErr(err)
	is.Equal(stat.Name(), "about")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// ReadDir duo
	df = dfs.New(nil, View())
	des, err = fs.ReadDir(df, "duo/view/about")
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "about.svelte")
	is.Equal(des[0].IsDir(), false)
	is.Equal(des[0].Type(), fs.FileMode(0))
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)

	// 4. duo/view/index.svelte
	// Open
	df = dfs.New(nil, View())
	file, err = df.Open("duo/view/index.svelte")
	is.NoErr(err)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "index.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// Stat
	df = dfs.New(nil, View())
	stat, err = fs.Stat(df, "duo/view/index.svelte")
	is.NoErr(err)
	is.Equal(stat.Name(), "index.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// ReadFile
	df = dfs.New(nil, View())
	code, err := fs.ReadFile(df, "duo/view/index.svelte")
	is.NoErr(err)
	is.Equal(string(code), `<h1>index</h1>`)

	// 4. duo/view/about/about.svelte
	// Open
	df = dfs.New(nil, View())
	file, err = df.Open("duo/view/about/about.svelte")
	is.NoErr(err)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "about.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// Stat
	df = dfs.New(nil, View())
	stat, err = fs.Stat(df, "duo/view/about/about.svelte")
	is.NoErr(err)
	is.Equal(stat.Name(), "about.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// ReadFile
	df = dfs.New(nil, View())
	code, err = fs.ReadFile(df, "duo/view/about/about.svelte")
	is.NoErr(err)
	is.Equal(string(code), `<h2>about</h2>`)
}

func TestInstance(t *testing.T) {
	is := is.New(t)
	df := dfs.New(nil, View())

	// 1. duo
	file, err := df.Open("duo")
	is.NoErr(err)
	rdf, ok := file.(fs.ReadDirFile)
	is.True(ok)
	des, err := rdf.ReadDir(-1)
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "view")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)
	fi, err := des[0].Info()
	is.NoErr(err)
	is.Equal(fi.IsDir(), true)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.ModeDir)
	is.Equal(fi.Name(), "view")
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	stat, err := file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "duo")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// Stat duo
	stat, err = fs.Stat(df, "duo")
	is.NoErr(err)
	is.Equal(stat.Name(), "duo")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// ReadDir duo
	des, err = fs.ReadDir(df, "duo")
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "view")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)

	// 2. duo/view
	file, err = df.Open("duo/view")
	is.NoErr(err)
	rdf, ok = file.(fs.ReadDirFile)
	is.True(ok)
	des, err = rdf.ReadDir(-1)
	is.NoErr(err)
	is.Equal(len(des), 2)
	is.Equal(des[0].Name(), "about")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about")
	is.Equal(fi.IsDir(), true)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.ModeDir)
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	is.Equal(des[1].Name(), "index.svelte")
	is.Equal(des[1].IsDir(), false)
	is.Equal(des[1].Type(), fs.FileMode(0))
	fi, err = des[1].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "index.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "view")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// Stat duo
	stat, err = fs.Stat(df, "duo/view")
	is.NoErr(err)
	is.Equal(stat.Name(), "view")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// ReadDir duo
	des, err = fs.ReadDir(df, "duo/view")
	is.NoErr(err)
	is.Equal(len(des), 2)
	is.Equal(des[0].Name(), "about")
	is.Equal(des[0].IsDir(), true)
	is.Equal(des[0].Type(), fs.ModeDir)
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about")
	is.Equal(fi.IsDir(), true)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.ModeDir)
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	is.Equal(des[1].Name(), "index.svelte")
	is.Equal(des[1].IsDir(), false)
	is.Equal(des[1].Type(), fs.FileMode(0))
	fi, err = des[1].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "index.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)

	// 3. duo/view/about
	file, err = df.Open("duo/view/about")
	is.NoErr(err)
	rdf, ok = file.(fs.ReadDirFile)
	is.True(ok)
	des, err = rdf.ReadDir(-1)
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "about.svelte")
	is.Equal(des[0].IsDir(), false)
	is.Equal(des[0].Type(), fs.FileMode(0))
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "about")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// Stat duo
	stat, err = fs.Stat(df, "duo/view/about")
	is.NoErr(err)
	is.Equal(stat.Name(), "about")
	is.Equal(stat.Mode(), fs.ModeDir)
	is.True(stat.IsDir())
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(0))
	is.Equal(stat.Sys(), nil)
	// ReadDir duo
	des, err = fs.ReadDir(df, "duo/view/about")
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "about.svelte")
	is.Equal(des[0].IsDir(), false)
	is.Equal(des[0].Type(), fs.FileMode(0))
	fi, err = des[0].Info()
	is.NoErr(err)
	is.Equal(fi.Name(), "about.svelte")
	is.Equal(fi.IsDir(), false)
	is.True(fi.ModTime().IsZero())
	is.Equal(fi.Mode(), fs.FileMode(0))
	is.Equal(fi.Size(), int64(0))
	is.Equal(fi.Sys(), nil)

	// 4. duo/view/index.svelte
	// Open
	file, err = df.Open("duo/view/index.svelte")
	is.NoErr(err)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "index.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// Stat
	stat, err = fs.Stat(df, "duo/view/index.svelte")
	is.NoErr(err)
	is.Equal(stat.Name(), "index.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// ReadFile
	code, err := fs.ReadFile(df, "duo/view/index.svelte")
	is.NoErr(err)
	is.Equal(string(code), `<h1>index</h1>`)

	// 4. duo/view/about/about.svelte
	// Open
	file, err = df.Open("duo/view/about/about.svelte")
	is.NoErr(err)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "about.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// Stat
	stat, err = fs.Stat(df, "duo/view/about/about.svelte")
	is.NoErr(err)
	is.Equal(stat.Name(), "about.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(14))
	is.Equal(stat.Sys(), nil)
	// ReadFile
	code, err = fs.ReadFile(df, "duo/view/about/about.svelte")
	is.NoErr(err)
	is.Equal(string(code), `<h2>about</h2>`)
}

func TestDirFS(t *testing.T) {
	is := is.New(t)
	df := dfs.New(os.DirFS("."), map[string]dfs.Generator{
		"duo/dfs/dfs.go": dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
			if err := dfs.Exists(f, "dfs.go"); err != nil {
				return err
			}
			file.Write([]byte(`package dfs`))
			return nil
		}),
		"duo/public/public.go": dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
			if err := dfs.Exists(f, "public/public.go"); err != nil {
				return err
			}
			file.Write([]byte(`package public`))
			return nil
		}),
	})
	code, err := fs.ReadFile(df, "duo/dfs/dfs.go")
	is.NoErr(err)
	is.Equal(string(code), `package dfs`)
	code, err = fs.ReadFile(df, "duo/public/public.go")
	is.Equal(err.Error(), "open duo/public/public.go > exists public/public.go > open public/public.go > file does not exist")
	is.Equal(code, nil)
	stat, err := fs.Stat(df, "dfs.go")
	is.NoErr(err)
	is.Equal(stat.Name(), "dfs.go")
	is.Equal(stat.IsDir(), false)
}
func TestGenerateFileError(t *testing.T) {
	is := is.New(t)
	df := dfs.New(os.DirFS("."), map[string]dfs.Generator{
		"duo/main.go": dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
			return fs.ErrNotExist
		}),
	})
	code, err := fs.ReadFile(df, "duo/main.go")
	is.True(err != nil)
	is.Equal(err.Error(), "open duo/main.go > file does not exist")
	is.Equal(code, nil)
}
func TestFileUnderneath(t *testing.T) {
	is := is.New(t)
	df := dfs.New(os.DirFS("."), map[string]dfs.Generator{
		"dfs.go": dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
			return fs.ErrNotExist
		}),
	})
	stat, err := fs.Stat(df, "dfs.go")
	is.True(err != nil)
	is.Equal(err.Error(), "open dfs.go > file does not exist")
	is.Equal(stat, nil)
}
func TestServeFile(t *testing.T) {
	is := is.New(t)
	df := dfs.New(os.DirFS("."), map[string]dfs.Generator{
		"duo/view": dfs.ServeFile(func(f dfs.FS, file *dfs.File) error {
			file.Write([]byte(file.Path() + `'s data`))
			return nil
		}),
	})
	des, err := fs.ReadDir(df, "duo/view")
	is.True(errors.Is(err, fs.ErrInvalid))
	is.Equal(len(des), 0)

	// _index.svelte
	file, err := df.Open("duo/view/_index.svelte")
	is.NoErr(err)
	stat, err := file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "_index.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(29))
	is.Equal(stat.Sys(), nil)
	code, err := fs.ReadFile(df, "duo/view/_index.svelte")
	is.NoErr(err)
	is.Equal(string(code), `duo/view/_index.svelte's data`)

	// about/_about.svelte
	file, err = df.Open("duo/view/about/_about.svelte")
	is.NoErr(err)
	stat, err = file.Stat()
	is.NoErr(err)
	is.Equal(stat.Name(), "_about.svelte")
	is.Equal(stat.Mode(), fs.FileMode(0))
	is.Equal(stat.IsDir(), false)
	is.True(stat.ModTime().IsZero())
	is.Equal(stat.Size(), int64(35))
	is.Equal(stat.Sys(), nil)
	code, err = fs.ReadFile(df, "duo/view/about/_about.svelte")
	is.NoErr(err)
	is.Equal(string(code), `duo/view/about/_about.svelte's data`)
}
func TestHTTP(t *testing.T) {
	is := is.New(t)
	df := dfs.New(os.DirFS("."), map[string]dfs.Generator{
		"duo/view": dfs.ServeFile(func(f dfs.FS, file *dfs.File) error {
			file.Write([]byte(file.Path() + `'s data`))
			return nil
		}),
	})
	hfs := http.FS(df)

	handler := func(w http.ResponseWriter, r *http.Request) {
		file, err := hfs.Open(r.URL.Path)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		stat, err := file.Stat()
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}
		w.Header().Add("Content-Type", "text/javascript")
		http.ServeContent(w, r, r.URL.Path, stat.ModTime(), file)
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/duo/view/_index.svelte", nil)
	handler(w, r)

	response := w.Result()
	body, err := ioutil.ReadAll(response.Body)
	is.NoErr(err)
	is.Equal(string(body), `duo/view/_index.svelte's data`)
	is.Equal(response.StatusCode, 200)
}

func TestDirects(t *testing.T) {
	t.SkipNow()
	// is := is.New(t)
	// dfs := dfs.New(nil, &dfs.Dir{
	// 	Entries: map[string]dfs.Generator{
	// 		"duo/view": &dfs.Dir{
	// 			Entries: map[string]dfs.Generator{
	// 				"index.svelte": &dfs.File{
	// 					Data: []byte(`<h1>index</h1>`),
	// 				},
	// 			},
	// 		},
	// 	},
	// })
	// code, err := fs.ReadFile(dfs, "duo/view/index.svelte")
	// is.NoErr(err)
	// is.Equal(string(code), "<h1>index</h1>")
}

func rootless(fpath string) string {
	parts := strings.Split(fpath, string(filepath.Separator))
	return path.Join(parts[1:]...)
}

func TestTargetPath(t *testing.T) {
	is := is.New(t)
	// Test inner file and rootless
	df := dfs.New(nil, map[string]dfs.Generator{
		"duo/view": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			is.Equal(dir.Path(), "duo/view")
			dir.Entry("about/about.svelte", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
				is.Equal(file.Path(), "duo/view/about/about.svelte")
				file.Write([]byte(rootless(file.Path())))
				return nil
			}))
			return nil
		}),
	})
	code, err := fs.ReadFile(df, "duo/view/about/about.svelte")
	is.NoErr(err)
	is.Equal(string(code), "view/about/about.svelte")
}

func TestDynamicDir(t *testing.T) {
	is := is.New(t)
	df := dfs.New(nil, map[string]dfs.Generator{
		"duo/view": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			doms := []string{"about/about.svelte", "index.svelte"}
			for _, dom := range doms {
				dom := dom
				dir.Entry(dom, dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
					file.Write([]byte(`<h1>` + dom + `</h1>`))
					return nil
				}))
			}
			return nil
		}),
	})
	des, err := fs.ReadDir(df, "duo/view")
	is.NoErr(err)
	is.Equal(len(des), 2)
	is.Equal(des[0].Name(), "about")
	is.Equal(des[1].Name(), "index.svelte")
	des, err = fs.ReadDir(df, "duo/view/about")
	is.NoErr(err)
	is.Equal(len(des), 1)
	is.Equal(des[0].Name(), "about.svelte")
}

func TestBases(t *testing.T) {
	is := is.New(t)
	df := dfs.New(nil, map[string]dfs.Generator{
		"duo/view": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			return nil
		}),
		"duo/controller": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			return nil
		}),
	})
	stat, err := fs.Stat(df, "duo/controller")
	is.NoErr(err)
	is.Equal(stat.Name(), "controller")
	stat, err = fs.Stat(df, "duo/view")
	is.NoErr(err)
	is.Equal(stat.Name(), "view")
}

func TestDirPath(t *testing.T) {
	is := is.New(t)
	df := dfs.New(nil, map[string]dfs.Generator{
		"duo/view": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			is.Equal(dir.Path(), "duo/view")
			dir.Entry("public", dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
				is.Equal(dir.Path(), "duo/view/public")
				dir.Entry("favicon.ico", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
					is.Equal(file.Path(), "duo/view/public/favicon.ico")
					file.Write([]byte("cool_favicon.ico"))
					return nil
				}))
				return nil
			}))
			return nil
		}),
		"duo": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			is.Equal(dir.Path(), "duo")
			dir.Entry("controller", dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
				is.Equal(dir.Path(), "duo/controller")
				dir.Entry("controller.go", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
					is.Equal(file.Path(), "duo/controller/controller.go")
					file.Write([]byte("package controller"))
					return nil
				}))
				return nil
			}))
			return nil
		}),
	})
	code, err := fs.ReadFile(df, "duo/view/public/favicon.ico")
	is.NoErr(err)
	is.Equal(string(code), "cool_favicon.ico")
	code, err = fs.ReadFile(df, "duo/controller/controller.go")
	is.NoErr(err)
	is.Equal(string(code), "package controller")
}

func TestWatchFile(t *testing.T) {
	is := is.New(t)
	df := dfs.New(os.DirFS("."), map[string]dfs.Generator{
		"duo/view": dfs.ServeFile(func(f dfs.FS, file *dfs.File) error {
			source := rootless(file.Path())
			file.Write([]byte(source + `'s data`))
			file.Watch(source, dfs.WriteEvent|dfs.RemoveEvent)
			return nil
		}),
	})
	subs, err := df.Subscribe("duo/view/index.svelte")
	is.NoErr(err)
	select {
	case <-subs.Wait():
		t.Fatal("No event expected")
	default:
	}
	df.Trigger("view/index.svelte", dfs.WriteEvent)
	select {
	default:
		t.Fatal("Write event expected")
	case event := <-subs.Wait():
		is.Equal(string(event), "Write")
	}
}
func TestWatchDir(t *testing.T) {
	is := is.New(t)
	df := dfs.New(os.DirFS("."), map[string]dfs.Generator{
		"duo/view": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			dir.Entry("index.svelte", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
				file.Write([]byte(`<h2>index</h2>`))
				return nil
			}))
			dir.Entry("about/about.svelte", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
				file.Write([]byte(`<h2>about</h2>`))
				return nil
			}))
			dir.Watch("view/{**,*}.{svelte,jsx}", dfs.CreateEvent|dfs.RemoveEvent)
			return nil
		}),
	})
	subs, err := df.Subscribe("duo/view")
	is.NoErr(err)
	select {
	case <-subs.Wait():
		t.Fatal("No event expected")
	default:
	}
	df.Trigger("view/edit.svelte", dfs.CreateEvent)
	select {
	default:
		t.Fatal("Write event expected")
	case event := <-subs.Wait():
		is.Equal(string(event), "Create")
	}
}
func TestDirMerge(t *testing.T) {
	is := is.New(t)
	df := dfs.New(nil, map[string]dfs.Generator{
		"duo/view": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			dir.Entry("index.svelte", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
				file.Write([]byte(`<h1>index</h1>`))
				return nil
			}))
			dir.Entry("somedir", dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
				return nil
			}))
			return nil
		}),
		"duo/view/view.go": dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
			file.Write([]byte(`package view`))
			return nil
		}),
		"duo/view/plugin.go": dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
			file.Write([]byte(`package plugin`))
			return nil
		}),
	})

	// duo/view
	des, err := fs.ReadDir(df, "duo/view")
	is.NoErr(err)
	is.Equal(len(des), 4)
	is.Equal(des[0].Name(), "index.svelte")
	is.Equal(des[0].IsDir(), false)
	is.Equal(des[1].Name(), "plugin.go")
	is.Equal(des[1].IsDir(), false)
	is.Equal(des[2].Name(), "somedir")
	is.Equal(des[2].IsDir(), true)
	is.Equal(des[3].Name(), "view.go")
	is.Equal(des[3].IsDir(), false)
}

func TestAddGenerator(t *testing.T) {
	is := is.New(t)
	// Add the view
	df := dfs.New(nil, View())

	// Add the controller
	df.Add(map[string]dfs.Generator{
		"duo/controller": dfs.GenerateDir(func(f dfs.FS, dir *dfs.Dir) error {
			dir.Entry("controller.go", dfs.GenerateFile(func(f dfs.FS, file *dfs.File) error {
				file.Write([]byte(`package controller`))
				return nil
			}))
			return nil
		}),
	})

	des, err := fs.ReadDir(df, "duo")
	is.NoErr(err)
	is.Equal(len(des), 2)
	is.Equal(des[0].Name(), "controller")
	is.Equal(des[1].Name(), "view")

	// Read from view
	code, err := fs.ReadFile(df, "duo/view/index.svelte")
	is.NoErr(err)
	is.Equal(string(code), `<h1>index</h1>`)

	// Read from controller
	code, err = fs.ReadFile(df, "duo/controller/controller.go")
	is.NoErr(err)
	is.Equal(string(code), `package controller`)
}
