package glob_test

import (
	"testing"

	"github.com/matryer/is"
	"gitlab.com/mnm/duo/intern/glob"
)

func TestSet(t *testing.T) {
	is := is.New(t)
	var set glob.Set
	is.Equal(len(set.List()), 0)
	err := set.Add("hi/*", "world/{**,*}", "hi/*")
	is.NoErr(err)
	is.Equal(len(set.List()), 2)
	is.Equal(set.List()[0].Match("hi/world"), true)
	is.Equal(set.List()[0].Match("hi"), false)
	is.Equal(set.List()[1].Match("world/"), true)
	is.Equal(set.List()[1].Match("world/cool/ok"), true)
	is.Equal(set.List()[1].Match("world/cool"), true)
}
