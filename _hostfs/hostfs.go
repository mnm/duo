// Package hostfs is a file system for reading virtual files from a host
// process.
package hostfs

import (
	"io"
	"io/fs"
	"net/rpc"
	"os"

	"gitlab.com/mnm/duo/virtual"
)

// New pipes
func New() fs.FS {
	reader := os.NewFile(4, "hostfs_read_pipe")
	writer := os.NewFile(5, "hostfs_write_pipe")
	conn := &conn{reader, writer}
	client := rpc.NewClient(conn)
	return &hfs{client}
}

type conn struct {
	rc io.ReadCloser
	wc io.WriteCloser
}

func (c *conn) Read(p []byte) (int, error) {
	return c.rc.Read(p)
}

func (c *conn) Write(p []byte) (int, error) {
	return c.wc.Write(p)
}

func (c *conn) Close() error {
	c.rc.Close()
	c.wc.Close()
	return nil
}

type hfs struct {
	client *rpc.Client
}

func (hfs *hfs) Open(name string) (fs.File, error) {
	var data []byte
	if err := hfs.client.Call("fs.Open", name, &data); err != nil {
		return nil, err
	}
	return virtual.File(name, data), nil
}
