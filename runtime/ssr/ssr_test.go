package ssr_test

import (
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"testing/fstest"

	"gitlab.com/mnm/duo/internal/npm"
	"gitlab.com/mnm/duo/runtime/ssr"
	"gitlab.com/mnm/duo/runtime/svelte"

	"github.com/matryer/is"
	"gitlab.com/mnm/duo/intern/vfs"
	v8 "gitlab.com/mnm/duo/js/v8"
	"gitlab.com/mnm/duo/runtime/dfs"
)

func TestSvelteView(t *testing.T) {
	is := is.New(t)
	cwd, err := os.Getwd()
	is.NoErr(err)
	dir := filepath.Join(cwd, "_tmp")
	is.NoErr(os.RemoveAll(dir))
	defer func() {
		if !t.Failed() {
			is.NoErr(os.RemoveAll(dir))
		}
	}()
	memfs := vfs.Memory{
		"view/index.svelte": &fstest.MapFile{
			Data: []byte(`<h1>hi world</h1>`),
		},
	}
	is.NoErr(vfs.WriteAll(".", dir, memfs))
	dirfs := os.DirFS(dir)
	svelte := svelte.New(&svelte.Input{
		VM:  v8.New(),
		Dev: true,
	})
	dfs := dfs.New(dirfs, map[string]dfs.Generator{
		"duo/view/_ssr.js": ssr.Generator(dirfs, svelte, dir),
	})
	// Install svelte
	err = npm.Install(dir, "svelte@3.42.3")
	is.NoErr(err)
	// Read the wrapped version of index.svelte with node_modules rewritten
	code, err := fs.ReadFile(dfs, "duo/view/_ssr.js")
	is.NoErr(err)
	is.True(strings.Contains(string(code), `create_ssr_component(`))
	is.True(strings.Contains(string(code), `<h1>hi world</h1>`))
	is.True(strings.Contains(string(code), `views["/"] = `))
}
