| duo | Search | Docs | Changelog |
| :-: | :----: | :--: | :-------: |

# Duo is a complete web framework written in Go

## Launch your web application faster by writing less code and making less decisions

| Email Address | Join the Waitlist |
| :-----------: | ----------------- |

We'll email you when it's ready. No spam ever.

## Learn how you'll launch faster from the creator

| Youtube Video |
| :-----------: |

## Features

### Integrated System

### Convention over Configuration

### Single Binary

#### Write High-Level, Compile to Low-Level

#### Pay as you Go

### Love and Care (Attention to Detail)

## FAQ

### Who am I?

- I've been building developer tools for the last decade.
- Some of my libraries are very popular

### Who's using Duo?

- Standup Jack
