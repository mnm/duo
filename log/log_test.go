package log_test

import (
	"os"
	"testing"

	"gitlab.com/mnm/duo/log"
	"gitlab.com/mnm/duo/log/logfmt"
)

func TestLog(t *testing.T) {
	log := log.New(logfmt.New(os.Stderr, log.InfoLevel))
	log.Debug("this is a debug message")
	// log.Info(log.Logfmt(log.Info))
	// t.Logf("hi world")
	// t.Errorf("hi world")
}
