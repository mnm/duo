package dfs

import (
	"io/fs"
	"time"
)

type EmbedFile struct {
	Data    []byte      // file content
	Mode    fs.FileMode // FileInfo.Mode
	ModTime time.Time   // FileInfo.ModTime
}

// Embedded filesystem. This is conceptually similar to fstest.MapFS,
// but doesn't try synthesizing subdirectories and doesn't support
// reading or walking directories.
type EFS map[string]*EmbedFile

func (efs EFS) Add(fs map[string]*EmbedFile) EFS {
	for path, file := range fs {
		efs[path] = file
	}
	return efs
}

func (efs EFS) Open(name string) (fs.File, error) {
	file, ok := efs[name]
	if !ok {
		return nil, fs.ErrNotExist
	}
	return &openFile{
		path:    name,
		data:    file.Data,
		mode:    file.Mode,
		modTime: file.ModTime,
	}, nil
}
