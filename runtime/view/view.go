package view

import (
	"encoding/json"
	"fmt"
	"io/fs"
	"net/http"
	"os"
	"strings"

	"gitlab.com/mnm/duo/js"
	"gitlab.com/mnm/duo/runtime/dfs"
	"gitlab.com/mnm/duo/runtime/dom"
	"gitlab.com/mnm/duo/runtime/ssr"
	"gitlab.com/mnm/duo/runtime/svelte"
)

type Response struct {
	Status  int               `json:"status,omitempty"`
	Headers map[string]string `json:"headers,omitempty"`
	Body    string            `json:"body,omitempty"`
}

func (res *Response) Write(w http.ResponseWriter) {
	// Write the response out
	for key, value := range res.Headers {
		w.Header().Set(key, value)
	}
	w.WriteHeader(res.Status)
	w.Write([]byte(res.Body))
}

// Renderer interface
type Renderer interface {
	Render(path string, props interface{}) (*Response, error)
}

// // FileServer serves the client-side files
// func FileServer(fs fs.FS) *Server {
// 	return &Server{http.FS(fs)}
// }

// type Server struct {
// 	hfs http.FileSystem
// }

// func (s *Server) Middleware(next http.Handler) http.Handler {
// 	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
// 		if !isClient(r.URL.Path) {
// 			next.ServeHTTP(w, r)
// 			return
// 		}
// 		s.ServeHTTP(w, r)
// 	})
// }

// func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
// 	file, err := s.hfs.Open(r.URL.Path)
// 	if err != nil {
// 		fmt.Println(err)
// 		http.Error(w, err.Error(), 500)
// 		return
// 	}
// 	stat, err := file.Stat()
// 	if err != nil {
// 		fmt.Println(err)
// 		http.Error(w, err.Error(), 500)
// 		return
// 	}
// 	w.Header().Add("Content-Type", "text/javascript")
// 	http.ServeContent(w, r, r.URL.Path, stat.ModTime(), file)
// }

func Runner(rootDir string, df *dfs.DFS, vm js.VM) *View {
	dirfs := os.DirFS(rootDir)
	svelte := svelte.New(&svelte.Input{
		VM:  vm,
		Dev: true,
	})
	// Add to the existing DFS
	df.Add(map[string]dfs.Generator{
		"duo/view":         dom.Runner(svelte, rootDir),
		"duo/node_modules": dom.NodeModules(rootDir),
		"duo/view/_ssr.js": ssr.Generator(dirfs, svelte, rootDir),
	})
	return &View{df, http.FS(df), vm}
}

func Builder(ef *dfs.EFS, vm js.VM) *View {
	return &View{ef, http.FS(ef), vm}
}

type View struct {
	fs  fs.FS
	hfs http.FileSystem
	vm  js.VM
}

func (v *View) Render(path string, props interface{}) (*Response, error) {
	propBytes, err := json.Marshal(props)
	if err != nil {
		return nil, err
	}
	script, err := fs.ReadFile(v.fs, "duo/view/_ssr.js")
	if err != nil {
		return nil, err
	}
	// Evaluate the server
	expr := fmt.Sprintf(`%s; duo.render(%q, %s)`, script, path, propBytes)
	result, err := v.vm.Eval("_ssr.js", expr)
	if err != nil {
		return nil, err
	}
	// Unmarshal the response
	res := new(Response)
	if err := json.Unmarshal([]byte(result), res); err != nil {
		return nil, err
	}
	return res, nil
}

func isClient(path string) bool {
	return strings.HasPrefix(path, "/duo/node_modules/") ||
		strings.HasPrefix(path, "/duo/view/")
}

func (v *View) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !isClient(r.URL.Path) {
			next.ServeHTTP(w, r)
			return
		}
		v.ServeHTTP(w, r)
	})
}

func (v *View) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	file, err := v.hfs.Open(r.URL.Path)
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	stat, err := file.Stat()
	if err != nil {
		fmt.Println(err)
		http.Error(w, err.Error(), 500)
		return
	}
	w.Header().Add("Content-Type", "text/javascript")
	http.ServeContent(w, r, r.URL.Path, stat.ModTime(), file)
}
